<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="description" content="bootstrap default admin template">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login | Imprenta y Gráficas Chone</title>

    <!-- START GLOBAL CSS -->
    <link href="{{ asset("assets/global/plugins/bootstrap/dist/css/bootstrap.min.css" ) }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset("assets/global/plugins/Waves/dist/waves.min.css") }}" type="text/css" rel="stylesheet"/>
    <!-- END GLOBAL CSS -->

    <!-- START PAGE PLUG-IN CSS -->
    <link rel="stylesheet" href="{{ asset("assets/icons_fonts/font-awesome/css/font-awesome.min.css") }}"/>
    <!-- END PAGE PLUG-IN CSS -->

    <!-- START TEMPLATE GLOBAL CSS -->
    <link href="{{ asset("assets/pages/login/css/user_login_v2.css") }}" type="text/css" rel="stylesheet"/>
    <!-- END TEMPLATE GLOBAL CSS -->

    <!-- Start favicon ico -->
    <link rel="icon" href="{{ asset("assets/favicon/prince.ico") }}" type="image/x-icon"/>
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset("assets/favicon/prince-192x192.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset("assets/favicon/prince-180x180.png") }}">
    <!-- End favicon ico -->

</head>
<body>

<div class="login-background">
    <div class="login-left-section">
        
        <h2>Imprenta y Gráficas Chone</h2>
        
    </div>
    <!--  START LOGIN -->
    <div class="login-page">
        <div class="main-login-contain">
            <div class="login-form">
                <form id="form-validation"  method="POST" action="{{ route('login') }}">
                     @csrf
                    <h4>Inicio de sesión</h4>
                    
                    <div class="form-group">
                        <input id="input-email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        
                        <label class="control-label" for="input-email">Correo electrónico</label><i class="bar"></i>
                    </div>
                    <div class="form-group">
                        <input id="input-password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        <label class="control-label" for="input-password">Contraseña</label><i class="bar"></i>
                    </div>

                    <div class="goto-login">
                        <div class="forgot-password-login">
                            
                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    <i class="icon icon_lock"></i>  {{ __('Recuperar contraseña') }}
                                </a>

                                 <a class="btn btn-link" href="{{ route('register') }}">
                                    <i class="fa fa-arrow-right"></i>  {{ __('Registrarse') }}
                                </a>
                            @endif
                        </div>
                     
                        <button type="submit" class="btn btn-login float-button-light">
                                    {{ __('Entrar') }}
                                </button>
                    </div>

                   
                </form>
            </div>
        </div>
    </div>
    <!--  END LOGIN -->
</div>

<!-- START CORE JAVASCRIPT -->
<script src="{{ asset("assets/global/plugins/jquery/dist/jquery.min.js") }}"></script>
<script src="{{ asset("assets/global/plugins/bootstrap/dist/js/bootstrap.min.js") }}"></script>
<script src="{{ asset("assets/global/plugins/Waves/dist/waves.min.js") }}"></script>
<!-- END CORE JAVASCRIPT -->

<!-- START PAGE JAVASCRIPT -->
<script>
    function openNav() {
        document.getElementById("myNav").style.width = "100%";
    }

    function closeNav() {
        document.getElementById("myNav").style.width = "0%";
    }

    !(function ($) {
        if (typeof Waves !== 'undefined') {
            Waves.attach('.float-button-light', ['waves-button', 'waves-float', 'waves-light']);
            Waves.init();
        }
    })(jQuery);
</script>
<!-- END PAGE JAVASCRIPT -->

</body>
</html>


