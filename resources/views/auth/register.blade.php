<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="description" content="bootstrap default admin template">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Register | Imprenta y Gráficas Chone </title>

    <!-- START GLOBAL CSS -->
    <link href="{{ asset("assets/global/plugins/bootstrap/dist/css/bootstrap.min.css" ) }}" type="text/css" rel="stylesheet"/>
    <link href="{{ asset("assets/global/plugins/Waves/dist/waves.min.css") }}" type="text/css" rel="stylesheet"/>
    <!-- END GLOBAL CSS -->

    <!-- START PAGE PLUG-IN CSS -->
    <link rel="stylesheet" href="{{ asset("assets/icons_fonts/font-awesome/css/font-awesome.min.css") }}"/>
    <!-- END PAGE PLUG-IN CSS -->

    <!-- START TEMPLATE GLOBAL CSS -->
    <link href="{{ asset("assets/pages/register/css/user_register_v2.css") }}" type="text/css" rel="stylesheet"/>
    <!-- END TEMPLATE GLOBAL CSS -->

    <!-- Start favicon ico -->
    <link rel="icon" href="{{ asset("assets/favicon/prince.ico") }}" type="image/x-icon"/>
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset("assets/favicon/prince-192x192.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset("assets/favicon/prince-180x180.png") }}">
    <!-- End favicon ico -->

</head>
<body>

<div class="register-background">
    <div class="register-left-section">
       
        <h2>Imprenta y Gráficas Chone</h2>
       
    </div>
    <!--  START REGISTER -->
    <div class="register-page">
        <div class="main-register-contain">
            <div class="register-form">
                <form id="form-validation" method="POST" action="{{ route('register') }}">
                     @csrf
                    <h4>Crear nueva cuenta</h4>
                    
                    <div class="form-group">
                         <input id="input-name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        <label class="control-label" for="input-name">Nombre</label><i class="bar"></i>
                    </div>
                    <div class="form-group">
                         <input id="input-email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        <label class="control-label" for="input-email">Correo Electrónico</label><i class="bar"></i>
                    </div>
                    <div class="form-group">
                        <input id="input-password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        <label class="control-label" for="input-password">Contraseña</label><i class="bar"></i>
                    </div>
                    <div class="form-group">
                        <input id="input-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        <label class="control-label" for="input-confirm">Confirmar Contraseña</label><i class="bar"></i>
                    </div>

                    <div class="goto-register">
                        <div class="checkbox">
                            <a class="btn btn-link" href="{{ route('login') }}">
                                    <i class="fa fa-arrow-right"></i>  {{ __('Ya tengo una cuenta') }}
                                </a>
                        </div>
                        <button type="submit" class="btn btn-register float-button-light"> {{ __('Registrarse') }}</button>
                    </div>



                </form>
            </div>
        </div>
    </div>
    <!--  END REGISTER -->
</div>

<!-- START CORE JAVASCRIPT -->
<script src="{{ asset("assets/global/plugins/jquery/dist/jquery.min.js") }}"></script>
<script src="{{ asset("assets/global/plugins/bootstrap/dist/js/bootstrap.min.js") }}"></script>
<script src="{{ asset("assets/global/plugins/Waves/dist/waves.min.js") }}"></script>
<!-- END CORE JAVASCRIPT -->

<!-- START PAGE JAVASCRIPT -->
<script>
    function openNav() {
        document.getElementById("myNav").style.width = "100%";
    }

    function closeNav() {
        document.getElementById("myNav").style.width = "0%";
    }

    !(function ($) {
        if (typeof Waves !== 'undefined') {
            Waves.attach('.float-button-light', ['waves-button', 'waves-float', 'waves-light']);
            Waves.init();
        }
    })(jQuery);
</script>
<!-- END PAGE JAVASCRIPT -->

</body>
</html>


