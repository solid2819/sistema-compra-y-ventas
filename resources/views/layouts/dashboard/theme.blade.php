<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="description" content="bootstrap default admin template">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sistema CV | @yield("title")</title>


    <!-- Start global css -->
    <link rel="stylesheet" href="{{ asset("assets/global/plugins/Waves/dist/waves.min.css") }}"/>
    <link rel="stylesheet" href="{{ asset("assets/global/plugins/bootstrap/dist/css/bootstrap.min.css") }}"/>
    <link rel="stylesheet" href="{{ asset("assets/icons_fonts/font-awesome/css/font-awesome.min.css") }}"/>
    <!-- End global css -->

    <!-- Start page plugin css -->
    <link rel="stylesheet" href="{{ asset("assets/global/plugins/jqvmap/dist/jqvmap.css") }}"/>
    <link rel="stylesheet" href="{{ asset("vendor/bootstrap-select/bootstrap-select.css") }}"/>
    <!-- End page plugin css -->

    <!-- Start template global css -->
    <link href="{{ asset("assets/global/css/components.min.css") }}" type="text/css" rel="stylesheet"/>
    <!-- End template global css -->

    <!-- Start layout css -->
    <link rel="stylesheet" href="{{ asset("assets/layouts/layouts_left_menu/left_menu_layout.min.css") }}"/>
    <!-- End layout css -->

    <!-- Start favicon ico -->
    <link rel="icon" href="{{ asset("assets/favicon/prince.ico") }}" type="image/x-icon"/>
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset("assets/favicon/prince-192x192.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset("assets/favicon/prince-180x180.png") }}">
    <!-- End favicon ico -->
    <style type="text/css" media="screen">
        .sidebar-system, .sidebar-footer, .sidebar-footer a, .main_container {
            background: #000000!important;
        }    
    </style>

    @yield("styles")
</head>
<body class="nav-medium">
<div class="container body">
    <div class="main_container">
            <!-- Start Loader -->
<div class="page-loader">
    <div class="preloader loading">
        <span class="slice"></span>
        <span class="slice"></span>
        <span class="slice"></span>
        <span class="slice"></span>
        <span class="slice"></span>
        <span class="slice"></span>
    </div>
</div>
<!-- End Loader -->

<!-- Start Scroll Top -->
<a href="javascript:" id="scroll" style="display: none;"><span></span></a>
<!-- End Scroll Top -->

<!-- start Left Menu-->
@include("layouts.dashboard.sidebar")
<!-- End Left Menu -->

<!-- start top navigation -->
@include("layouts.dashboard.header")
<!-- End top navigation -->

    <!-- Start Contain Section -->
    <div class="container-fluid right_color">
        @yield("content")
    </div>
    <!-- End Contain Section -->

    <!-- Start Footer Section -->
@include("layouts.dashboard.footer")
   @yield("scripts")

</body>
</html>