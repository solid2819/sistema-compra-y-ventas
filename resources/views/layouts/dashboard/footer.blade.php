 <footer class="footer">
        © 2019 Todos los derechos reservados <a class="text-primary" href="">Imprenta y Gráficas Chone</a>
    </footer>
    <!-- End Footer Section -->
</div>
</div>

<!-- Start core js -->
<script src="{{ asset("assets/global/plugins/jquery/dist/jquery.min.js") }}"></script>
<script src="{{ asset("assets/global/plugins/bootstrap/dist/js/bootstrap.min.js") }}"></script>
<script src="{{ asset("assets/global/plugins/Waves/dist/waves.min.js") }}"></script>
<script src="{{ asset("assets/global/plugins/screenfull.js/dist/screenfull.min.js") }}"></script>
<script src="{{ asset("assets/global/plugins/jquery-slimscroll/jquery.slimscroll.j") }}s"></script>
<!-- Start core javascript -->

<!-- Start global js -->
<script src="{{ asset("assets/global/js/left-menu.min.js") }}"></script>
<!-- End global js -->

<!-- Start page plugin js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{ asset("assets/global/plugins/chart.js/dist/Chart.min.js") }}"></script>
<script src="{{ asset("assets/global/plugins/chart.js/samples/utils.js") }}"></script>
<script src="{{ asset("assets/global/plugins/jquery.sparkline/jquery.sparkline.js") }}"></script>
<script src="{{ asset("assets/global/plugins/jqvmap/dist/jquery.vmap.js") }}"></script>
<script src="{{ asset("assets/global/plugins/jqvmap/dist/maps/jquery.vmap.world.js") }}"></script>
<script src="{{ asset("assets/global/plugins/jqvmap/examples/js/jquery.vmap.sampledata.js") }}"></script>
<script src="{{ asset("vendor/bootstrap-select/bootstrap-select.js") }}"></script>
<!-- End page plugin js -->

<!-- Start page js -->
<script src="{{ asset("assets/global/js/dashboard_v1.min.js") }}"></script>
<!-- End page js -->