<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                
                <div class="responsive-logo">
                    <a href="{{ route("dashboard") }}">
                        <img src="{{ asset("assets/global/images/prince_logo.png") }}" alt="main-logo">
                    </a>
                </div>
            </div>

            <div class="topbar-right">
                <div class="nav navbar-nav navbar-right">

                    <div class="dropdown user-profile right-icon">
                        <a href="javascript:" class="dropdown-toggle waves-effect waves-light"
                           data-toggle="dropdown"
                           aria-expanded="false">
                            {{-- <img src="{{ asset("assets/global/images/user10.jpg") }}" alt="user"> --}}
                            <span class="fa fa-user"></span>
                        </a>
                        <ul class="dropdown-menu">
                           <li>
                                <a href="#" class="waves-effect waves-light">
                                <i class="fa fa-user" aria-hidden="true"></i>Bienvenido {{ Auth()->user()->name}}</a>
                            </li>

                           
                           
                            <li role="separator" class="divider"></li>
                           @if(Auth::user())
                                    <li><a class="waves-effect waves-light" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-power-off text-danger" aria-hidden="true"></i> {{ __('Logout') }}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                        </form>
                                    </li>
                                @endif
                        </ul>
                    </div>

                </div>

            </div>
        </nav>
    </div>
</div>
<div class="clearfix"></div>