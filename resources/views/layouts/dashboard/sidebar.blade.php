
<div class="col-md-3 left_color sidebar-system">
    <div class="left_color scroll-view">
        <div class="navbar nav_title">
            <h6 class="logo-letras" style="color: white; margin: 10px;">Imprenta y Gráficas Chone</h6>
            {{-- <a href="{{ route("dashboard") }}" class="small-logo">
                <img src="{{ asset("assets/global/images/prince_logo2.png") }}" alt="small-logo">
            </a> --}}
        </div>

        <div class="clearfix"></div>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Menú</h3>
                <ul class="nav side-menu">
                    <li>
                        <a class="waves-effect waves-light" href="{{ route("dashboard") }}">
                            <i class="fa fa-home"></i> Principal
                        </a>
                    </li>
                    <li>
                        <a href="#" class="waves-effect waves-light">
                            <i class="fa fa-sitemap"></i> 
                            Categorías <span class="fa fa-chevron-right"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li> 
                                <a class="waves-effect waves-light" href="{{ url("dashboard/categories/create") }}">
                                    Nueva
                                </a>
                            </li>
                            <li> 
                                <a class="waves-effect waves-light" href="{{ url("dashboard/categories") }}">
                                    Ver Categorías
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="waves-effect waves-light">
                            <i class="fa fa-cube"></i> Productos 
                            <span class="fa fa-chevron-right"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li> 
                                <a class="waves-effect waves-light" href="{{ url("dashboard/products/create") }}">
                                    Nuevo
                                </a>
                            </li>
                            <li> 
                                <a class="waves-effect waves-light" href="{{ url("dashboard/products") }}">
                                    Ver Productos
                                </a>
                            </li>
                        </ul>
                    </li>

                     {{--  <li>
                        <a class="waves-effect waves-light">
                            <i class="fa fa-book"></i> Ventas 
                            <span class="fa fa-chevron-right"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li> 
                                <a class="waves-effect waves-light" href="{{ url("dashboard/ventas/personas") }}">
                                    Clientes
                                </a>
                            </li>
                             <li> 
                                <a class="waves-effect waves-light" href="{{ url("dashboard/ventas/ventas-clientes") }}">
                                    Ventas realizadas
                                </a>
                            </li>
                        </ul>
                    </li> --}}

                      <li>
                        <a class="waves-effect waves-light">
                            <i class="fa fa-shopping-cart"></i> Compras 
                            <span class="fa fa-chevron-right"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li> 
                                <a class="waves-effect waves-light" href="{{ url("dashboard/compras/personas/providers") }}">
                                    Proveedores
                                </a>
                            </li>
                            <li> 
                                <a class="waves-effect waves-light" href="{{ url("dashboard/compras/ingresos") }}">
                                    Registros de compras
                                </a>
                            </li>
                        </ul>
                    </li>


                      <li>
                        <a class="waves-effect waves-light">
                            <i class="fa fa-user"></i> Acceso 
                            <span class="fa fa-chevron-right"></span>
                        </a>
                        <ul class="nav child_menu">
                         <li> 
                                <a class="waves-effect waves-light" href="{{ url("dashboard/users/") }}">
                                    Usuarios del sistema
                                </a>
                            </li>
                            <li> 
                                <a class="waves-effect waves-light" href="{{ url("dashboard/users/create") }}">
                                    Crear nuevo usuario
                                </a>
                            </li>
                        </ul>
                    </li>
                   
                  
                </ul>
            </div>
           {{--  <div class="menu_section">
                <h3>Live On</h3>
                <ul class="nav side-menu">
                    <li><a class="waves-effect waves-light"><i class="fa fa-anchor"></i> Icons <span
                                    class="fa fa-chevron-right"></span></a>
                        <ul class="nav child_menu">
                            <li><a class="waves-effect waves-light" href="font_awesome.html">Font awesome</a></li>
                            <li><a class="waves-effect waves-light" href="material_icon.html">Material icon</a></li>
                            <li><a class="waves-effect waves-light" href="themify_icon.html">Themify icon</a></li>
                            <li><a class="waves-effect waves-light" href="flag_icon.html">Flag icon</a></li>
                        </ul>
                    </li>
                    <li><a class="waves-effect waves-light"><i class="fa fa-map-marker"></i> Maps <span
                                    class="fa fa-chevron-right"></span></a>
                        <ul class="nav child_menu">
                            <li><a class="waves-effect waves-light" href="map-google.html">Google Maps</a></li>
                            <li><a class="waves-effect waves-light" href="map-vector.html">Vector Map</a></li>
                        </ul>
                    </li>
                    <li><a class="waves-effect waves-light"><i class="fa fa-user-circle-o"></i> User Pages <span
                                    class="fa fa-chevron-right"></span></a>
                        <ul class="nav child_menu">
                            <li><a class="waves-effect waves-light" href="user_login_v1.html">Login 1</a></li>
                            <li><a class="waves-effect waves-light" href="user_login_v2.html">Login 2</a></li>
                            <li><a class="waves-effect waves-light" href="user_register_v1.html">Register 1</a></li>
                            <li><a class="waves-effect waves-light" href="user_register_v2.html">Register 2</a></li>
                            <li><a class="waves-effect waves-light" href="user_forgot_password_v1.html">Forgot 1</a></li>
                            <li><a class="waves-effect waves-light" href="user_forgot_password_v2.html">Forgot 2</a></li>
                            <li><a class="waves-effect waves-light" href="user_lockscreen_v1.html">Lockscreen 1</a></li>
                            <li><a class="waves-effect waves-light" href="user_lockscreen_v2.html">Lockscreen 2</a></li>
                        </ul>
                    </li>
                    <li><a class="waves-effect waves-light"><i class="fa fa-exclamation-circle"></i> Error Pages <span
                                    class="fa fa-chevron-right"></span></a>
                        <ul class="nav child_menu">
                            <li><a class="waves-effect waves-light" href="user_404.html">Error 404</a></li>
                            <li><a class="waves-effect waves-light" href="user_400.html">Error 400</a></li>
                            <li><a class="waves-effect waves-light" href="user_403.html">Error 403</a></li>
                            <li><a class="waves-effect waves-light" href="user_405.html">Error 405</a></li>
                            <li><a class="waves-effect waves-light" href="user_500.html">Error 500</a></li>
                            <li><a class="waves-effect waves-light" href="user_503.html">Error 503</a></li>
                        </ul>
                    </li>
                    <li><a class="waves-effect waves-light"><i class="fa fa-balance-scale"></i> General Pages <span
                                    class="fa fa-chevron-right"></span></a>
                        <ul class="nav child_menu">
                            <li><a class="waves-effect waves-light" href="faq.html">FAQ</a></li>
                            <li><a class="waves-effect waves-light" href="userlist.html">User List</a></li>
                            <li><a class="waves-effect waves-light" href="invoice.html">Invoice</a></li>
                            <li><a class="waves-effect waves-light" href="blank.html">Blank</a></li>
                            <li><a class="waves-effect waves-light" href="profile.html">Profile</a></li>
                            <li><a class="waves-effect waves-light" href="gallery.html">Gallery</a></li>
                            <li><a class="waves-effect waves-light" href="maintenance.html">Maintenance</a></li>
                            <li><a class="waves-effect waves-light" href="draggable_grid.html">Draggable Grid</a></li>
                            <li><a class="waves-effect waves-light" href="grids.html">Grids</a></li>
                            <li><a class="waves-effect waves-light" href="search_results.html">Search result</a></li>
                        </ul>
                    </li>
                    <li><a class="waves-effect waves-light" href="documentation.html"><i class="fa fa-cogs"></i> Documentation</a></li>
                </ul>
            </div> --}}

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small sidebar-system">
            {{-- <a data-toggle="tooltip" data-placement="top" title="Configuración">
                <span class="fa fa-cog" aria-hidden="true"></span>
            </a> --}}
           {{--  <a class="toggle-fullscreen" data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="fa fa-arrows-alt" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="fa fa-lock" aria-hidden="true"></span>
            </a> --}}
           
            @if(Auth::user())
                <a data-toggle="tooltip" data-placement="top" title="Logout" class="waves-effect waves-light pull-right" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                <i class="fa fa-power-off " aria-hidden="true"></i> </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
                </form>
            @endif
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>