@if(count($errors)>0)
	<div class="alert alert-danger alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">×</span>
		</button>
	    <p class="icon-text"><i class="fa fa-info-circle" aria-hidden="true"></i>¡Error!</p>
			 <ul>
				@foreach($errors->all() as $error)
			     	<li>{{ $error }}</li>
			    @endforeach
		    </ul>
	</div>
@endif

@if(session("info"))
	<div class="alert alert-info alert-dismissible" role="alert">
	    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">×</span>
		</button>
	    <p class="icon-text"><i class="fa fa-info-circle" aria-hidden="true"></i> Información</p>
		{!! session("info") !!}
	</div>
@endif