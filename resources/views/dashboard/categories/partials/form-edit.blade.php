<div class="form-item">
	<p class="formLabel">Nombre de la categoría</p>
	{{ Form::text("name",null, ["class" => "form-style", "id" => "name_cat", "autocomplete" => "off"])}} 
</div>
								{{-- 
			                <label>Gender</label>
			                <div class="control-group">
			                    <label class="control control-radio control-inline">Male
			                    <input type="radio" name="radio2" checked="checked"/>
			                    <span class="control-indicator"></span>
			                    </label>
			                    <label class="control control-radio control-inline">Female
			                    <input type="radio" name="radio2"/>
			                             <span class="control-indicator"></span>
			                         </label>
			                </div> --}}

{{-- <div class="form-item">
	<p class="formLabel">Slug</p>
	{{ Form::text("slug",null, ["class" => "form-style", "id" => "slug_cat", "autocomplete" => "off"])}} 
</div> --}}

<div class="form-item">
	<p class="formLabel">Descripción</p>
	{{ Form::textarea("description",null, ["class" => "form-style", "id" => "textarea-cat", "autocomplete" => "off"])}} 
		                        	
	</textarea>
</div>

<button type="submit" class="btn btn-primary btn-outline float-button-light waves-effect waves-button waves-float waves-light">
	Actualizar
</button>
<a href="{{ url("dashboard/categories") }}">
	<button type="button" class="btn btn-warning btn-outline float-button-light waves-effect waves-button waves-float waves-light">
		Volver
	</button>
</a>