<div class="form-item">
	<p {{-- class="formLabel" --}}>Nombre de la categoría</p>
	{{ Form::text("name",null, ["class" => "form-style", "id" => "name_cat", "autocomplete" => "off"])}} 
</div>
<div class="form-item">
	<p {{-- class="formLabel" --}}>Descripción</p>
	{{ Form::textarea("description",null, ["class" => "form-style", "id" => "textarea-cat", "autocomplete" => "off"])}} 
		                        	
	</textarea>
</div>

<button type="submit" class="btn btn-primary btn-outline float-button-light waves-effect waves-button waves-float waves-light">
	Crear
</button>
<button type="reset" class="btn btn-inverse btn-outline float-button-light waves-effect waves-button waves-float waves-light">
	Borrar
</button>
<a href="{{ url("dashboard/categories") }}">
	<button type="button" class="btn btn-warning btn-outline float-button-light waves-effect waves-button waves-float waves-light">
		Volver
	</button>
</a>

@section("scripts")
  <script src="{{ asset("vendor/jquery.stringToSlug/jquery.stringToSlug.min.js") }} "></script>
  
  <script>
    $(document).ready(function () {
      $("#name_cat, #slug_cat").stringToSlug({
        callback: function (text) {
          $("#slug_cat").val(text);
        }
      });
    });
  </script>
@endsection