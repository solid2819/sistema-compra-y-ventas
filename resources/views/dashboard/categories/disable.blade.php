@extends("layouts.dashboard.theme")
@section("title")
	Categorías inactivas
@endsection

@section("styles")
	<style type="text/css" media="screen">
		.btn1{
			width: 5rem!important;
		}
	</style>
@endsection

@section("content")
	<div class="contain-inner-section">
		  <div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Categorías</h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                <li class="text-info">Almacen</li>
                <li class="active">Categorías</li>
            </ol>
        </div>
    	@include("layouts.dashboard.message"){{--message--}}
        <div class="row">
            <div class="">
	            <div class="section-header">
	                <a href="{{ url("dashboard/categories/create") }}">
	                	<button type="button" class="btn btn-primary btn-outline float-button-light">
	                        <i class="fa fa-plus"> </i> Nueva
	                 	</button>
	                </a>
	            </div>
	            @include("dashboard.categories.partials.search")
	            <div class="section-body">
	                <div class="table-responsive">
	                   @if(count($categories->status=="Desactivada")) 
	                     <table class="table table-hover">
		                        <thead>
		                        <tr>
		                            <th>#</th>
		                            <th>Categoría</th>
		                            <th>Descripción</th>
		                            <th>Estado</th>
		                            <th>Fecha de creación</th>
		                            <th>Última actualización</th>
		                        </tr>
		                        </thead>
		                        <tbody>
		                        @foreach($categories as $category)
			                        <tr class="text-capitalize">
			                            <th scope="row">
			                                        	{{ $category->id }}
			                            </th>
			                            <td>{{ $category->name }}
			                             <br>
			                             <br>
			                             <a href="{{ route("categories.show" , $category->id) }}">
			                               <button type="button" class="btn1 btn btn-primary btn-outline btn-xs float-button-light">
			                                    <span class="fa fa-eye"> Ver</span>
			                               </button>
			                             </a>
			                             <a href="{{ route("categories.edit" , $category->id) }}">
			                               <button type="button" class="btn1 btn btn-success btn-outline btn-xs float-button-light">
			                                    <span class="fa fa-exchange"></span> Modificar
			                               </button>
			                             </a>
			                             <a href="#">
			                              <button type="button" data-target="#modal-delete-{{ $category->id }}" data-toggle="modal" class="btn1 btn btn-danger btn-outline btn-xs float-button-light">
			                               		<span class="fa fa-trash"></span> Eliminar
			                              </button>
			                             </a>
			                            </td>
			                            <td>
			                            	@if($category->description)
			                            		{{ $category->description }}
			                            	@else
			                            		N/D
			                            	@endif
			                            </td>
			                            <td>{{ $category->status }}</td>
			                            <td>{{ $category->created_at }}</td>
			                            <td>{{ $category->updated_at }}</td>
			                        </tr>
			                        @include("dashboard.categories.partials.modal_delete")
		                        @endforeach
		                        </tbody>
		                    </table>
	                    @else
	                        No hay categorías  
	                       	<a href="{{ url("dashboard/categories") }}">
		                       	 <button type="button" class="btn1 btn btn-info btn-outline btn-xs float-button-light" data-target="#danger-modal" data-toggle="modal">
				                    <span class="fa fa-arrow-left"></span> Regresar
				                </button>
	                       </a>
	                    @endif
	                </div>
	                {{ $categories->render() }}
	            </div>
	        </div>
        </div>
    </div>
@endsection

@section("scripts")
	<script src="{{ asset("assets/global/js/form_input.min.js") }}"></script>
@endsection