@extends("layouts.dashboard.theme")
@section("title")
	Nueva categorías
@endsection

@section("styles")
	<style type="text/css">
		#textarea-cat{
			height: 10rem;
			max-width: 31.5rem;
			max-height: 31.5rem;
		}
	</style>
@endsection

@section("content")
	<div class="contain-inner-section">
		<div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Nueva categoría</span></h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                <li class="text-info">Almacen</li>
                 <li><a href="{{ url("dashboard/categories") }}">Categories</a></li>
                <li class="active">New</li>
            </ol>
        </div>

    	@if(count($errors)>0)
    		@include("layouts.dashboard.message"){{--message--}}
    	@endif
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
	                <div class="section-header">
	                	<h2>Crear categoría</h2>
	                </div>
	                <div class="section-body">
		                {!! Form::open(array("url"=>"dashboard/categories", "method"=>"POST", "autocomplete"=>"off")) !!}
		                {!! Form::token() !!}
			                @include("dashboard.categories.partials.form-create")
		               {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
	<script src="{{ asset("assets/global/js/form_input.min.js") }}"></script>
@endsection