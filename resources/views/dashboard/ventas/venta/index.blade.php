@extends("layouts.dashboard.theme")
@section("title")
	Registros de ventas
@endsection

@section("styles")
	<style type="text/css" media="screen">
		.btn1{
			width: 6rem!important;
		}
	</style>
@endsection

@section("content")
	<div class="contain-inner-section">
		  <div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Registros de ventas</h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                 <li class="text-info">Ventas</li>
                <li class="active">Ventas a clientes</li>
            </ol>
        </div>
    	@include("layouts.dashboard.message"){{--message--}}
        <div class="row">
            <div class="">
	            <div class="section-header">
	                <a href="{{ url("dashboard/ventas/ventas-clientes/create") }}">
	                	<button type="button" class="btn btn-primary btn-outline float-button-light">
	                        <i class="fa fa-plus"> </i> Nueva venta
	                 	</button>
	                </a>
	            </div>
	            @include("dashboard.ventas.venta.partials.search")
	            <div class="section-body">
	                <div class="table-responsive">
	                   @if(count($ventas)) 
	                     <table class="table table-hover">
		                        <thead>
		                        <tr>
		                            <th>#</th>
		                            <th>Fecha de ingreso</th>
		                            <th>Cliente</th>
		                            <th>Comprobante</th>
		                            <th>Nro de Serie</th>
		                            {{-- <th>Nro de Comprobante</th> --}}
		                            <th>Impuesto</th>
		                            <th>Estado</th>
		                            <th>Precio Total</th>
		                            <th>Opciones</th>
		                        </tr>
		                        </thead>
		                        <tbody>
		                        @foreach($ventas as $venta)
			                        <tr class="text-capitalize">
			                            <th scope="row">{{ $venta->idventa }}
			                            </th>

			                            <td>{{ $venta->date }}
			                            </td>

			                            <td>{{ $venta->name }}
			                            </td>

			                            <td>{{ $venta->tipo_voucher ." nro:". $venta->serie_voucher }}
			                            </td>

			                            <td>{{ $venta->nro_voucher }}
			                            </td>

			                            <td>{{ $venta->impuesto . " Bs"}}
			                            </td>

			                            <td>{{ $venta->status }}
			                            </td>

			                            <td>{{ $venta->total . " Bs"}}</td>

			                            <td>
				                             <a href="{{ route("venta.show" , $venta->idventa) }}">
				                               <button type="button" class="btn1 btn btn-primary btn-outline btn-xs float-button-light">
				                                    <span class="fa fa-eye"> Ver detalle</span>
				                               </button>
				                             </a>
				                            {{--  <a href="{{ route("venta.edit" , $vent->id) }}">
				                               <button type="button" class="btn1 btn btn-success btn-outline btn-xs float-button-light">
				                                    <span class="fa fa-exchange"></span> Modificar
				                               </button>
				                             </a> --}}
				                             <a href="#">
				                              <button type="button" data-target="#modal-delete-{{ $venta->idventa }}" data-toggle="modal" class="btn1 btn btn-danger btn-outline btn-xs float-button-light">
				                               		<span class="fa fa-trash"></span> Anular
				                              </button>
				                             </a>
				                         </td>
			                        </tr>
			                        @include("dashboard.ventas.venta.partials.modal_delete")
		                        @endforeach
		                        </tbody>
		                    </table>
	                    @else
	                        No hay registros disponibles 
	                       	@if($searchText)
	                       		<a href="{{ url("dashboard/ventas/venta-clientes") }}">
			                       	 <button type="button" class="btn1 btn btn-info btn-outline btn-xs float-button-light" data-target="#danger-modal" data-toggle="modal">
					                    <span class="fa fa-arrow-left"></span> Regresar
					                </button>
	                       		</a>
	                       	@endif
	                    @endif
	                </div>
	                {{ $ventas->render() }}
	            </div>
	        </div>
        </div>
    </div>
@endsection

{{-- @section("scripts")
	<script src="{{ asset("assets/global/js/form_input.min.js") }}"></script>
@endsection --}}