@extends("layouts.dashboard.theme")
@section("title")
	Registrar nueva venta
@endsection

@section("content")
	<div class="contain-inner-section">
		<div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Nueva venta</span></h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                <li class="text-info">Ventas</li>
                
                <li class="active">New</li>
            </ol>
        </div>

    	@if(count($errors)>0)
    		@include("layouts.dashboard.message"){{--message--}}
    	@endif
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                <div class="section-header">
	                	<h2>Registro de Venta</h2>
	                </div>
	                <div class="section-body">
		                {!! Form::open(array("url"=>"dashboard/ventas/ventas-clientes", "method"=>"POST", "autocomplete"=>"off")) !!}
		                {!! Form::token() !!}
			                @include("dashboard.ventas.venta.partials.form-create")
                        <input name="_token"  value="{{ csrf_token() }}" type="hidden" />
		               {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

