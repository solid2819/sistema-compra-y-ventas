<div class="modal fade" id="modal-delete-{{ $venta->idventa }}">
    {{{ Form::Open(array("action" => array("Admin\VentaController@destroy", $venta->idventa), "method" => "DELETE")) }}}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header danger-modal">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Cancelar venta</h4>
            </div>
            <div class="modal-body">
                <p>¿Estás seguro de anular esta venta {{ $venta->nro_voucher }}?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default float-button-light" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger float-button-light">Si</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>


 