<div class="modal fade" id="modal-delete-{{ $persona->idpersona }}">
    {{{ Form::Open(array("action" => array("Admin\ClientController@destroy", $persona->idpersona), "method" => "DELETE")) }}}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header danger-modal">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Desactivar Cliente</h4>
            </div>
            <div class="modal-body">
                <p>¿Estás seguro de eliminar a  {{ $persona->name }}?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default float-button-light" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger float-button-light">Si</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>


 