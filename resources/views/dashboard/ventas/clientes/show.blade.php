@extends("layouts.dashboard.theme")
@section("title")
	Cliente: {{ $persona->name }}
@endsection

@section("styles")
	<style type="text/css">
		#textarea-cat{
			height: 10rem;
			max-width: 31.5rem;
			max-height: 31.5rem;
		}
	</style>
@endsection

@section("content")
	<div class="contain-inner-section">
		<div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Cliente {{ $persona->name }}</span></h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                <li class="text-info">Almacen</li>
                 <li><a href="{{ url("dashboard/ventas/personas/") }}">Clients</a></li>
                <li class="active">Show</li>
            </ol>
        </div>

    	@if(count($errors)>0)
    		@include("layouts.dashboard.message"){{--message--}}
    	@endif
        <div class="row">
        	 	<div class="section-header">
	                <a href="{{ url("dashboard/ventas/personas/") }}">
	                	<button type="button" class="btn btn-warning btn-outline float-button-light">
	                        <i class="fa fa-arrow-left"> </i> Volver
	                 	</button>
	                </a>
	            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section-body">
	              
	                 <div class="">
                    
                    <div class="row">
                        <div class="col-md-8">
                            <div class="profile-left-section">
                                <ul class="nav nav-pills">
                                    <li class="active"><a data-toggle="tab" href="#info" class="float-button-light">Información del Cliente</a>
                                    </li>
                                   
                                </ul>
                                <div class="tab-content">
                                    <div id="info" class="tab-pane fade in active">
                                        <div class="section-body">
                                            <div class="profile-body">
                                           
                                            <div class="row">
                                                <div class="prosonal-info">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <p><span>Nombre del Cliente:- </span> {{ $persona->name}}</p>
                                                    <p><span>Tipo de documento </span> {{ $persona->document }} </p>
                                                    <p><span>Nro. de Documento </span> {{ $persona->nro_document }} </p> 
                                                    <p><span>Email:- </span> {{ $persona->email}}</p>
                                                    <p><span>Teléfono:- </span> {{ $persona->phone }}</p>

                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    
                                                    <p><span>Agregado al sistema :- </span> {{ $persona->created_at}}</p>
                                                    <p><span>Última actualización:- </span> {{ $persona->updated_at}}</p>
                                                    <p><span>Dirección:- </span> {{ $persona->address }}</p>
                                                   
                                                </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                       
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
	<script src="{{ asset("assets/global/js/form_input.min.js") }}"></script>
@endsection