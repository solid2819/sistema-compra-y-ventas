@extends("layouts.dashboard.theme")
@section("title")
	Clientes
@endsection

@section("styles")
	<style type="text/css" media="screen">
		.btn1{
			width: 6rem!important;
		}
	</style>
@endsection

@section("content")
	<div class="contain-inner-section">
		  <div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Clientes</h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                 <li class="text-info">Ventas</li>
                <li class="active">Clientes</li>
            </ol>
        </div>
    	@include("layouts.dashboard.message"){{--message--}}
        <div class="row">
            <div class="">
	            <div class="section-header">
	                <a href="{{ url("dashboard/ventas/personas/create") }}">
	                	<button type="button" class="btn btn-primary btn-outline float-button-light">
	                        <i class="fa fa-plus"> </i> Nuevo
	                 	</button>
	                </a>
	            </div>
	            @include("dashboard.ventas.clientes.partials.search")
	            <div class="section-body">
	                <div class="table-responsive">
	                   @if(count($personas)) 
	                     <table class="table table-hover">
		                        <thead>
		                        <tr>
		                            <th>#</th>
		                            <th>Nombre</th>
		                            <th>Email</th>
		                            <th>Tipo de documento</th>
		                            <th>Nro de Documento</th>
		                            <th>Registrado el</th>
		                            <th>Última actualización</th>
		                        </tr>
		                        </thead>
		                        <tbody>
		                        @foreach($personas as $persona)
			                        <tr class="text-capitalize">
			                            <th scope="row">
			                            {{ $persona->idpersona }}
			                            </th>
			                            <td>{{ $persona->name }}
			                             <br>
			                             <br>
			                             <a href="{{ route("personas.show" , $persona->idpersona) }}">
			                               <button type="button" class="btn1 btn btn-primary btn-outline btn-xs float-button-light">
			                                    <span class="fa fa-eye"> Ver</span>
			                               </button>
			                             </a>
			                             <a href="{{ route("personas.edit" , $persona->idpersona) }}">
			                               <button type="button" class="btn1 btn btn-success btn-outline btn-xs float-button-light">
			                                    <span class="fa fa-exchange"></span> Modificar
			                               </button>
			                             </a>
			                             <a href="#">
			                              <button type="button" data-target="#modal-delete-{{ $persona->idpersona }}" data-toggle="modal" class="btn1 btn btn-danger btn-outline btn-xs float-button-light">
			                               		<span class="fa fa-trash"></span> Eliminar
			                              </button>
			                             </a>
			                            </td>
			                            <td>
			                            	{{ $persona->email }}
			                            </td>
			                            <td>{{ $persona->document }}</td>
			                            <td>{{ $persona->nro_document }}</td>
			                          {{--   <td>{{ $persona->phone }}</td>
			                            <td>{{ $persona->address }}</td> --}}
			                            <td>{{ $persona->created_at }}</td>
			                            <td>{{ $persona->updated_at }}</td>
			                        </tr>
			                        @include("dashboard.ventas.clientes.partials.modal_delete")
		                        @endforeach
		                        </tbody>
		                    </table>
	                    @else
	                        No hay clientes registrados  
	                       	@if($searchText)
	                       		<a href="{{ url("dashboard/ventas/personas") }}">
			                       	 <button type="button" class="btn1 btn btn-info btn-outline btn-xs float-button-light" data-target="#danger-modal" data-toggle="modal">
					                    <span class="fa fa-arrow-left"></span> Regresar
					                </button>
	                       		</a>
	                       	@endif
	                    @endif
	                </div>
	                {{ $personas->render() }}
	            </div>
	        </div>
        </div>
    </div>
@endsection

{{-- @section("scripts")
	<script src="{{ asset("assets/global/js/form_input.min.js") }}"></script>
@endsection --}}