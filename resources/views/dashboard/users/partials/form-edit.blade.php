@section("styles")
	<style type="text/css" media="screen">
		#stock_product{
			width: 200px!important;
		}	
	</style>
@endsection
<div class="row">
	{{-- {{ Form::hidden("user", auth()->user()->id) }} --}}
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> {{-- columna Título y slug de producto --}}
			<div class="form-group">
				<label class="" for="input-name">Nombre</label><i class="bar"></i>
                         <div class="form-item">
							
							{{ Form::text("name",null, ["class" => "form-style", "id" => "name_cat", "autocomplete" => "off"])}} 
						</div>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        
                    </div>
                    <div class="form-group">
                    	<label class="" for="input-email">Correo Electrónico</label><i class="bar"></i>
                        <div class="form-item">
							
							{{ Form::email("email",null, ["class" => "form-style", "id" => "name_cat", "autocomplete" => "off"])}} 
						</div>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        
                    </div>
                    <div class="form-group">
                    	 <label class="" for="input-password">Contraseña</label><i class="bar"></i>
                       <div class="form-item">
							
							{{ Form::password("password",null, ["class" => "form-style", "id" => "name_cat", "autocomplete" => "off"])}} 
						</div>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                       
                    </div>
                    <div class="form-group">
                    	 <label class="" for="input-confirm">Confirmar Contraseña</label><i class="bar"></i>
                       <div class="form-item">
							
							{{ Form::password("password_confirmation",null, ["class" => "form-style", "id" => "name_cat", "autocomplete" => "off"])}} 
						</div>
                       
                    </div>

	
</div>
</div>

	
<button type="submit" class="btn btn-success btn-outline float-button-light waves-effect waves-button waves-float waves-light">
	Actualizar
</button>

<a href="{{ url("dashboard/products") }}">
	<button type="button" class="btn btn-warning btn-outline float-button-light waves-effect waves-button waves-float waves-light">
		Volver
	</button>
</a>

@section("scripts")
  <script src="{{ asset("vendor/jquery.stringToSlug/jquery.stringToSlug.min.js") }} "></script>
  <script src="{{ asset("vendor/ckeditor/ckeditor.js") }} "></script>
  
  <script>
   //  $(document).ready(function () {
   //    $("#name_pro, #slug_pro").stringToSlug({
   //      callback: function (text) {
   //        $("#slug_pro").val(text);
   //      }
   //    });
   //  });

   //  /*CK EDITOR*/
	  // CKEDITOR.config.height=400;
	  // CKEDITOR.config.width="auto";

	  // CKEDITOR.replace("textarea_pro");
  </script>
@endsection