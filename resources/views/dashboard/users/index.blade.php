@extends("layouts.dashboard.theme")
@section("title")
	Productos
@endsection

@section("styles")
	<style type="text/css" media="screen">
		.btn1{
			width: 6rem!important;
		}
		.img-pro{
			width: 8vw!important;
			height: 8vw!important;
		}
	</style>
@endsection

@section("content")
	<div class="contain-inner-section">
		  <div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Usuarios del sistema</h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                
                <li class="active">Users</li>
            </ol>
        </div>
    	@include("layouts.dashboard.message"){{--message--}}
        <div class="row">
            <div class="">
	            <div class="section-header">
	                <a href="{{ url("dashboard/users/create") }}">
	                	<button type="button" class="btn btn-primary btn-outline float-button-light">
	                        <i class="fa fa-plus"> </i> Agregar usuario al sistema
	                 	</button>
	                </a>
	            </div>
	            @include("dashboard.products.partials.search")
	            <div class="section-body">
	                <div class="table-responsive">
	                   @if(count($users))
	                     <table class="table table-hover">
		                        <thead>
		                        <tr>
		                        	<th>#</th>
		                        	
		                            <th>Nombre del usuario</th>
		                              <th>Email</th>
		                            {{-- <th>Código</th> --}}
		                           
		                            {{-- <th>Fecha de creación</th>
		                            <th>Última actualización</th> --}}
		                        </tr>
		                        </thead>
		                        <tbody>
		                        @foreach($users as $user)
			                        <tr class="text-capitalize">
			                        	 <th scope="row">
			                            	{{ $user->id }}
			                            </th>
			                        	{{-- <td>
			                        		@if($user->file)
			                        		<img src="{{ asset("images/users/" . $user->file) }}"  alt="{{ $user->name }}" class="img-thumbnail img-pro">
			                        		@else
			                        		<i class="fa fa-image"></i>
			                        		@endif
			                        	</td>	 --}}
			                            <td>{{ $user->name }}
			                             <br>
			                             <br>
			                             <a href="{{ route("users.show" , $user->id) }}">
			                               <button type="button" class="btn1 btn btn-primary btn-outline btn-xs float-button-light">
			                                    <span class="fa fa-eye"> Ver</span>
			                               </button>
			                             </a>
			                             <a href="{{ route("users.edit" , $user->id) }}">
			                               <button type="button" class="btn1 btn btn-success btn-outline btn-xs float-button-light">
			                                    <span class="fa fa-exchange"></span> Modificar
			                               </button>
			                             </a>
			                             <a href="#">
			                              <button type="button" data-target="#modal-delete-{{ $user->id }}" data-toggle="modal" class="btn1 btn btn-danger btn-outline btn-xs float-button-light">
			                               		<span class="fa fa-trash"></span> Eliminar
			                              </button>
			                             </a>
			                            </td>
			                             <td>{{ $user->email }}</td>
			                           {{--  <td>{{ $user->code }}</td>
			                            <td>{{ $user->stok }}</td>
			                            <td>{{ $user->status }}</td> --}}
			                            {{-- <td>{{ $product->created_at }}</td>
			                            <td>{{ $product->updated_at }}</td> --}}
			                        </tr>
			                        @include("dashboard.users.partials.modal_delete")
		                        @endforeach
		                        </tbody>
		                  </table>
		                  <a href="{{ route("downpdfall") }}" target="_blank" ><button class="pull-right btn-link">Descargar en PDF</button></a>
	                    @else
	                        No hay usuarios registrados  
	                       	@if($searchText)
	                       		<a href="{{ url("dashboard/users") }}">
			                       	 <button type="button" class="btn1 btn btn-info btn-outline btn-xs float-button-light" data-target="#danger-modal" data-toggle="modal">
					                    <span class="fa fa-arrow-left"></span> Regresar
					                </button>
	                       		</a>
	                       	@endif
	                    @endif
	                </div>
	                {{ $users->render() }}
	                
	            </div>
	        </div>
        </div>
    </div>
@endsection