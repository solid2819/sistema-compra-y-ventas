@extends("layouts.dashboard.theme")
@section("title")
  Usuario: {{ $user->name }}
@endsection

@section("styles")
  <style type="text/css">
    #textarea-cat{
      height: 10rem;
      max-width: 31.5rem;
      max-height: 31.5rem;
    }
  </style>
@endsection

@section("content")
  <div class="contain-inner-section">
    <div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Usuario del sistema </span></h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
               
                 <li><a href="{{ url("dashboard/compras/personas/providers") }}">Users</a></li>
                <li class="active">Show</li>
            </ol>
        </div>

      @if(count($errors)>0)
        @include("layouts.dashboard.message"){{--message--}}
      @endif
        <div class="row">
            <div class="section-header">
                  <a href="{{ url("dashboard/users/") }}">
                    <button type="button" class="btn btn-warning btn-outline float-button-light">
                          <i class="fa fa-arrow-left"> </i> Volver
                    </button>
                  </a>
              </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section-body">
                
                   <div class="">
                    
                    <div class="row">
                        <div class="col-md-8">
                            <div class="profile-left-section">
                                <ul class="nav nav-pills">
                                    <li class="active"><a data-toggle="tab" href="#info" class="float-button-light">Información del usuario</a>
                                    </li>
                                   
                                </ul>
                                <div class="tab-content">
                                    <div id="info" class="tab-pane fade in active">
                                        <div class="section-body">
                                            <div class="profile-body">
                                           
                                            <div class="row">
                                                <div class="prosonal-info">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <p><span>Nombre del Usuario:- </span> {{ $user->name}}</p>
                                                    {{-- <p><span>Tipo de documento </span> {{ $user->document }} </p>
                                                    <p><span>Nro. de Documento </span> {{ $user->nro_document }} </p>  --}}
                                                    <p><span>Email:- </span> {{ $user->email}}</p>
                                                   {{--  <p><span>Teléfono:- </span> {{ $user->phone }}</p> --}}

                                                </div>
                                                {{-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    
                                                    <p><span>Agregado al sistema :- </span> {{ $user->created_at}}</p>
                                                    <p><span>Última actualización:- </span> {{ $user->updated_at}}</p>
                                                    <p><span>Dirección:- </span> {{ $user->address }}</p>
                                                   
                                                </div> --}}
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                       
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
  <script src="{{ asset("assets/global/js/form_input.min.js") }}"></script>
@endsection