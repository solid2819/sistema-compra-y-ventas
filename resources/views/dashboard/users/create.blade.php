@extends("layouts.dashboard.theme")
@section("title")
	Nuevo usuario
@endsection

@section("styles")
@endsection

@section("content")
	<div class="contain-inner-section">
		<div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Nuevo Usuario</span></h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
              
                 <li><a href="{{ url("dashboard/products") }}">Users</a></li>
                <li class="active">New</li>
            </ol>
        </div>

    	@if(count($errors)>0)
    		@include("layouts.dashboard.message"){{--message--}}
    	@endif
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
	                <div class="section-header">
	                	<h2>Crear Nuevo Usuario</h2>
	                </div>
	                <div class="section-body">
		                {!! Form::open(array("url"=>"dashboard/users", "method"=>"POST", "autocomplete"=>"off", "files" => "true")) !!}
		                {{-- {!! Form::token() !!} --}}
			                @include("dashboard.users.partials.form-create")
		               {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection