@extends("layouts.dashboard.theme")
@section("title")
	Vista previa de Ingresos
@endsection

@section("styles")
	<style type="text/css">
		#textarea-cat{
			height: 10rem;
			max-width: 31.5rem;
			max-height: 31.5rem;
		}
	</style>
@endsection

@section("content")
	<div class="contain-inner-section">
		<div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Compra con  Nro de Factura <span class="text-danger">000000{!! $ingreso->nro_voucher !!}</span></h1>
            
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                <li class="text-info">Almacen</li>
                 <li><a href="{{ url("dashboard/categories") }}">Ingresos</a></li>
                <li class="active">View</li>
            </ol>
        </div>

       <div class="row">
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
		<div class="form-item">
			<p {{-- class="formLabel" --}}>Proveedor</p>

			<p>{{$ingreso->name}}</p> 

			<p class="pull-right">Fecha de creación {{ $ingreso->date, date('d-m-Y') }}</p>
		</div>
	</div>

	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		

		<div class="form-item">
			<p {{-- class="formLabel" --}}> Tipo de comprobante</p>
		    <span>{{ $ingreso->tipo_voucher }}</span>
		</div>


	</div>
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

		<div class="form-item">
			<p {{-- class="formLabel" --}}>Nro. del comprobante</p>
			 <span>{{ $ingreso->nro_voucher }}</span>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div class="form-item">
			<p {{-- class="formLabel" --}}>Nro. de serie</p>
			 <span>{{ $ingreso->serie_voucher }}</span>
		</div>
	</div>	
		
</div>

<div class="row">
	<div class="panel panel-primary">
		<div class="panel-body">
			<div class="section-header">
	         <h2>Detalles de la compra</h2>
	        </div>
			
	
			{{-- tabla de detalles agreagdo --}}
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
				<table id="detalles" class="table table-condensed table-striped table-bordered table-hover">
					<thead class="bg-info">
						<tr>
							
							<th>Producto</th>
							<th>Cantidad</th>
							<th>Precio compra</th>
							<th>Precio venta</th>
							<th>Subtotal</th>
						</tr>
					</thead>
					
					<tfoot>
						<tr>

							<th></th>
							<th></th>
							<th></th>
							
							
							<th>Total</th>
							<th><h4 id="total" class="text-danger">{{ $ingreso->total }}bs</h4></th>
						</tr>
					</tfoot>
					<tbody>
						@foreach($detalles as $det)
						<tr>
							<th>{{ $det->articulo }}</th>
							<th>{{ $det->cantidad }}</th>
							<th>{{ $det->precio_compra }}</th>
							<th>{{ $det->precio_venta }}</th>
							<th>{{ $det->cantidad*$det->precio_compra }} Bs</th>

						</tr>
						@endforeach
						
					</tbody>
				</table>
				{{-- end tabla de detalles agreagdo --}}
			</div>

		</div>
	</div>
</div>
    	<h4><a class="text-danger" href="{{ route("fac-compra", $ingreso->idingreso) }}" target="_blank">Generar factura</a></h4>
    	
    </div>
@endsection

@section("scripts")
	<script src="{{ asset("assets/global/js/form_input.min.js") }}"></script>
@endsection