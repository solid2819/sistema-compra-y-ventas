@section("styles")
	<style type="text/css">
		#text_address{
			height: 10rem;
			max-width: 31.5rem;
			max-height: 31.5rem;
			min-width: 31.5rem;
			min-height: 10rem;
		}
	</style>
@endsection

<div class="row">
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
		<div class="form-item">
			<p {{-- class="formLabel" --}}>Proveedor</p>

			 {{ Form::select("idproveedor", $proveedores, null , ["class" => "form-control selectpicker", "id" => "ingreso_provider_id", "data-live-search" => "true"])}} 
		</div>
	</div>

	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		

		<div class="form-item">
			<p {{-- class="formLabel" --}}> Tipo de comprobante</p>
		    <select name="tipo_voucher" class="form-style" value="{{old("tipo_voucher")}}" required>
		    	<option ></option>
		    	<option value="Factura">Factura</option>
		    	<option value="Ticket">Ticket</option>
		    	<option value="Nota de Entrega">Nota de entrega</option>
		    </select>
		</div>


	</div>
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

		<div class="form-item">
			<p {{-- class="formLabel" --}}>Nro. del comprobante</p>
			<input class="form-style" name="nro_voucher" id="nro_voucher" autocomplete="off" type="number" value="{{old("nro_voucher")}}" required>
			{{-- {{ Form::number("stok",null, ["class" => "form-style", "id" => "stock_product", "autocomplete" => "off"])}}  --}}
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div class="form-item">
			<p {{-- class="formLabel" --}}>Nro. de serie</p>
			<input class="form-style" name="serie_voucher" id="serie_voucher" autocomplete="off" type="number" value="{{old("serie_voucher")}}" required>
			{{-- {{ Form::number("stok",null, ["class" => "form-style", "id" => "stock_product", "autocomplete" => "off"])}}  --}}
		</div>
	</div>	
		
</div>

<div class="row">
	<div class="panel panel-primary">
		<div class="panel-body">
			<div class="section-header">
	         <h2>Detalles de la compra</h2>
	        </div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"> 
				<div class="form-item">
					<p {{-- class="formLabel" --}}>Producto</p>
					<select name="pidarticulo" class="form-control selectpicker" id="pidarticulo" data-live-search="true">
						@foreach($articulos as $articulo)
							<option value="{{ $articulo->idarticulo }}" >
								{{ $articulo->articulo }}
							</option>
						@endforeach
					</select> 
				</div>
			</div>


			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> 
				<div class="form-item">
					<p {{-- class="formLabel" --}}>Cantidad</p>
					<input type="number" name="pcantidad" id="pcantidad" class="form-control" maxlength="10" max="10">
				</div>
			</div>

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> 
				<div class="form-item">
					<p {{-- class="formLabel" --}}>Precio compra</p>
					<input type="number" name="pprecio_compra" id="pprecio_compra" class="form-control">
				</div>
			</div>

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> 
				<div class="form-item">
					<p {{-- class="formLabel" --}}>Precio venta</p>
					<input type="number" name="pprecio_venta" id="pprecio_venta" class="form-control">
				</div>
			</div>

			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"> 
				<div class="form-item">
					<p {{-- class="formLabel" --}}>&nbsp</p>
					<button type="button" id="bt_add" class="btn btn-success btn-outline btn-xs float-button-light">
					<i class="fa fa-plus"></i>
					Agregar al detalle</button>
				</div>
			</div>


			{{-- tabla de detalles agreagdo --}}
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
				<table id="detalles" class="table table-condensed table-striped table-bordered table-hover">
					<thead class="bg-info">
						<tr>
							<th>&nbsp</th>
							<th>Producto</th>
							<th>Cantidad</th>
							<th>Precio compra</th>
							<th>Precio venta</th>
							<th>Subtotal</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>Total</th>
							<th><h4 id="total">Bs 0.00</h4></th>
						</tr>
					</tfoot>
				</table>
				{{-- end tabla de detalles agreagdo --}}
			</div>

		</div>
	</div>
</div>

{{-- <div class="form-item">
	<p class="formLabel">Detalle adicional </p>
	{{ Form::textarea("detalle_adicional",null, ["class" => "form-style", "id" => "text_description", "autocomplete" => "off"])}} 
		                        	
	</textarea>
</div> --}}

<div id="guardar">
	<button type="submit" class="btn btn-success btn-outline float-button-light waves-effect waves-button waves-float waves-light">
	Guardar
</button>
<button type="reset" class="btn btn-inverse btn-outline float-button-light waves-effect waves-button waves-float waves-light">
	Borrar
</button>
<a href="{{ url("dashboard/compras/ingresos") }}">
	<button type="button" class="btn btn-warning btn-outline float-button-light waves-effect waves-button waves-float waves-light">
		Volver
	</button>
</a>
</div>

@section("scripts")
  <script>
   
   $(document).ready(function() {
   	$("#bt_add").click(function() {
   		agregar();
   	});
   });

   var cont=0; 
   total=0; 
   subtotal=[]; 

   $("#guardar").hide();


   function agregar() {
   		idarticulo=$("#pidarticulo").val();

   		articulo=$("#pidarticulo option:selected").text();
   		cantidad=$("#pcantidad").val();
   		precio_compra=$("#pprecio_compra").val();
   		precio_venta=$("#pprecio_venta").val();


   		if (idarticulo!="" && cantidad!="" && cantidad>0  && precio_compra!="" && precio_venta!="") {

   			subtotal[cont]=(cantidad*precio_compra);
   			total=(total+subtotal[cont]);

   			var fila = '<tr class="selected" id="fila'+cont+'"><td><button type="button"  class=" btn btn-danger btn-outline btn-xs float-button-light" onclick="eliminar('+cont+');"><span class="fa fa-trash"></span> </i></button></td><td><input type="hidden" name="idarticulo[]" value="'+idarticulo+'" />'+articulo+'</td><td><input type="number" name="cantidad[]" value="'+cantidad+'" /></td><td><input type="number" name="precio_compra[]" value="'+precio_compra+'" /></td><td><input type="number" name="precio_venta[]" value="'+precio_venta+'" /></td><td>'+subtotal[cont]+'</td><tr>';

   			cont++;

   			limpiar();

   			$("#total").html("Bs. " + (total));
   			evaluar();
   			$("#detalles").append(fila);
   		}else{
   			alert("Error al ingresar los detalles de compra. Compruebe que los campos no queden vacíos");
   		}
   }


  	function limpiar() {
  		$("#pcantidad").val("");
  		$("#pprecio_compra").val("");
  		$("#pprecio_venta").val("");
  	}


    function evaluar() {
    	if (total=>0) {
    		$("#guardar").show();
    	}
    	else{
    		$("#guardar").hide();
    	}
    }

    function eliminar(index) {
    	total=total-subtotal[index];
    	$("#total").html("Bs " + total);
    	$("#fila" + index).remove();
    	evaluar();
    }
  </script>


@endsection