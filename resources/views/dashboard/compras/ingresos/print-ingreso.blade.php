 <style type="text/css" media="screen">
 	table, p{
 		border: 1px solid #000
 	}

 </style>
<h1>Imprenta y Gráficas Chone</h1>
 <h3 class="page-name-title" style="text-align: right;">Compra con  Nro de Factura <span style="color: red">000000{!! $ingreso->nro_voucher !!}</span></h3>


 <table>
 	<caption><p></p></caption>
 	<thead>
 		<tr>
 			<th><span>{{ $ingreso->tipo_voucher }} Nro: <span class="text-danger">000000{!! $ingreso->nro_voucher !!}</span></span></th>

 			<th>Nro de Serie de <span>{{ $ingreso->tipo_voucher }} : {{ $ingreso->serie_voucher }}</span></th>
 		</tr>
 		
 		
 	</thead>
 	<tbody>
 		<thead>
 			<tr>
 				<th>Detalles / Descripción de la compra</th>
 			</tr>
 		</thead>
 		
 		<tbody >
 			<tr>
 			<td><tr>
 			<th>Compra al Proveedor: <span style="text-transform: uppercase;">{{$ingreso->name}}</span></th>
 		</tr></td>
 		</tr>
 		<tr>
 							<th>Nombre del Producto</th>
							<th>Cantidad</th>
							<th>Precio compra</th>
							<th>Precio venta</th>
							<th>Subtotal</th>
 		</tr>
						@foreach($detalles as $det)
						<tr>
							<th>{{ $det->articulo }}</th>
							<th>{{ $det->cantidad }}</th>
							<th>{{ $det->precio_compra }}</th>
							<th>{{ $det->precio_venta }}</th>
							<th>{{ $det->cantidad*$det->precio_compra }} Bs</th>

						</tr>
						@endforeach
						
					</tbody>
					<tfoot>
						<tr>

							<th></th>
							<th></th>
							<th></th>
							
							
							<th>Total</th>
							<th><h4 id="total" style="color:red; font-weight: bold;">{{ $ingreso->total }}bs</h4></th>
						</tr>
					</tfoot>
 	</tbody>

 </table>