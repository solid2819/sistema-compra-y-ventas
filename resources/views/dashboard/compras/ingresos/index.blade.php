@extends("layouts.dashboard.theme")
@section("title")
	Registros de compras
@endsection

@section("styles")
	<style type="text/css" media="screen">
		.btn1{
			width: 6rem!important;
		}
	</style>
@endsection

@section("content")
	<div class="contain-inner-section">
		  <div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Registros de compras</h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                 <li class="text-info">Compras</li>
                <li class="active">Ingresos</li>
            </ol>
        </div>
    	@include("layouts.dashboard.message"){{--message--}}
        <div class="row">
            <div class="">
	            <div class="section-header">
	                <a href="{{ url("dashboard/compras/ingresos/create") }}">
	                	<button type="button" class="btn btn-primary btn-outline float-button-light">
	                        <i class="fa fa-plus"> </i> Nuevo ingreso
	                 	</button>
	                </a>
	            </div>
	            @include("dashboard.compras.ingresos.partials.search")
	            <div class="section-body">
	                <div class="table-responsive">
	                   @if(count($ingresos)) 
	                     <table class="table table-hover">
		                        <thead>
		                        <tr>
		                            <th>#</th>
		                            <th>Fecha de ingreso</th>
		                            <th>Proveedor</th>
		                            <th>Comprobante</th>
		                            <th>Nro de Serie</th>
		                            {{-- <th>Nro de Comprobante</th> --}}
		                            <th>Impuesto</th>
		                            <th>Estado</th>
		                            <th>Precio Total</th>
		                            <th>Opciones</th>
		                        </tr>
		                        </thead>
		                        <tbody>
		                        @foreach($ingresos as $ingreso)
			                        <tr class="text-capitalize">
			                            <th scope="row">{{ $ingreso->idingreso }}
			                            </th>

			                            <td>{{ $ingreso->date }}
			                            </td>

			                            <td>{{ $ingreso->name }}
			                            </td>

			                            <td class=""><a class="text-danger" href="{{ route("ingresos.show" , $ingreso->idingreso) }}">{{ $ingreso->tipo_voucher ." nro: 000000". $ingreso->serie_voucher }}</a>
			                            </td>

			                            <td>{{ $ingreso->nro_voucher }}
			                            </td>

			                            <td>{{ $ingreso->impuesto . " Bs"}}
			                            </td>

			                            <td>{{ $ingreso->status }}
			                            </td>

			                            <td>{{ $ingreso->total . " Bs"}}</td>

			                            <td>
				                             <a href="{{ route("ingresos.show" , $ingreso->idingreso) }}">
				                               <button type="button" class="btn1 btn btn-primary btn-outline btn-xs float-button-light">
				                                    <span class="fa fa-eye"> Ver detalle</span>
				                               </button>
				                             </a>
				                            {{--  <a href="{{ route("ingresos.edit" , $ingreso->idingreso) }}">
				                               <button type="button" class="btn1 btn btn-success btn-outline btn-xs float-button-light">
				                                    <span class="fa fa-exchange"></span> Modificar
				                               </button>
				                             </a> --}}
				                             <a href="#">
				                              <button type="button" data-target="#modal-delete-{{ $ingreso->idingreso }}" data-toggle="modal" class="btn1 btn btn-danger btn-outline btn-xs float-button-light">
				                               		<span class="fa fa-trash"></span> Anular
				                              </button>
				                             </a>
				                         </td>
			                        </tr>
			                        @include("dashboard.compras.ingresos.partials.modal_delete")
		                        @endforeach
		                        </tbody>
		                    </table>
	                    @else
	                        No hay registros disponibles 
	                       	@if($searchText)
	                       		<a href="{{ url("dashboard/compras/ingresos") }}">
			                       	 <button type="button" class="btn1 btn btn-info btn-outline btn-xs float-button-light" data-target="#danger-modal" data-toggle="modal">
					                    <span class="fa fa-arrow-left"></span> Regresar
					                </button>
	                       		</a>
	                       	@endif
	                    @endif
	                </div>
	                {{ $ingresos->render() }}
	            </div>
	        </div>
        </div>
    </div>
@endsection

{{-- @section("scripts")
	<script src="{{ asset("assets/global/js/form_input.min.js") }}"></script>
@endsection --}}