<div class="" style="margin-top: 1rem!important;">
	<small {{-- class="formLabel" --}}>Buscar proveedor por nombre, nro. de documento</small>
	<form role="search" method="get" action="{{ url("dashboard/compras/personas/providers") }}">
	    <div class="form-group">
	        <div class="input-group">
		      <span class="input-group-btn">
		        <button class="btn btn-success" type="submit"><i class="fa fa-search"></i></button>
		      </span>
	           <input name="searchText" type="text" class="form-control" placeholder="Search..." value="{{$searchText}}">
	        </div>
	    </div>
	</form>
 </div>

{{-- {!! Form::open(array("url"=>"almacen/category", "method"=>"get", "autocomplete"=>"off", "role"=>"search")) !!} --}}