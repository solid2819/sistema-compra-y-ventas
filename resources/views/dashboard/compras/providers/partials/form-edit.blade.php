@section("styles")
	<style type="text/css">
		#text_address{
			height: 10rem;
			max-width: 31.5rem;
			max-height: 31.5rem;
			min-width: 31.5rem;
			min-height: 10rem;
		}
	</style>
@endsection

<div class="row">
	
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> 
		<div class="form-item">
			<p {{-- class="formLabel" --}}>Nombre del cliente</p>
			{{ Form::text("name",null, ["class" => "form-style", "id" => "name_cli", "autocomplete" => "off", "value" => "__old('name')__"])}} 
		</div>
								
		

		<div class="form-item">
			<p {{-- class="formLabel" --}}>Email</p>
			{{ Form::email("email",null, ["class" => "form-style", "id" => "email", "autocomplete" => "off"])}} 
		</div>
	</div>{{-- end columna Título y slug de producto --}}

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

		{{-- Categoría del producto --}}
		<div class="form-item">
			<p {{-- class="formLabel" --}}> Documento</p>
		    <select name="document" class="form-style">

		    	@if($persona->document=="RIF")
		    	<option value="RIF" selected="RIF">RIF</option>
		    	<option value=""></option>
		    	<option value="Cédula">Cédula</option>
		    	

		    	@else
		    	<option value="Cédula" selected="Cédula">Cédula</option>
		    	<option value=""></option>
		    	<option value="RIF" >RIF</option>
		    	
		    	

		    	@endif
		    </select>
		</div>

		
		<div class="form-item">
			<p {{-- class="formLabel" --}}>Nro documento</p>
			{{ Form::number("nro_document", null, ["class" => "form-style", "id" => "nro_document", "autocomplete" => "off"]) }}
			
		</div>

		
		<div class="form-item">
			<p {{-- class="formLabel" --}}>Teléfono</p>
			{{ Form::number("phone", null, ["class" => "form-style", "id" => "phone", "autocomplete" => "off"]) }}
		</div>
	</div>
</div>

<div class="form-item">
	<p {{-- class="formLabel" --}}>Dirección</p>
	{{ Form::textarea("address",null, ["class" => "form-style", "id" => "text_address", "autocomplete" => "off"]) }} 
		                        	
	</textarea>
</div>

<button type="submit" class="btn btn-success btn-outline float-button-light waves-effect waves-button waves-float waves-light">
	Actualizar
</button>

<a href="{{ url("dashboard/compras/personas/providers") }}">
	<button type="button" class="btn btn-warning btn-outline float-button-light waves-effect waves-button waves-float waves-light">
		Volver
	</button>
</a>

@section("scripts")
  <script src="{{ asset("vendor/jquery.stringToSlug/jquery.stringToSlug.min.js") }} "></script>
 {{--  <script src="{{ asset("vendor/ckeditor/ckeditor.js") }} "></script> --}}
  
  <script>
    $(document).ready(function () {
      $("#name_cli, #slug_cli").stringToSlug({
        callback: function (text) {
          $("#slug_cli").val(text);
        }
      });
    });

    /*CK EDITOR*/
	  // CKEDITOR.config.height=400;
	  // CKEDITOR.config.width="auto";

	  // CKEDITOR.replace("textarea_pro");
  </script>
@endsection