@extends("layouts.dashboard.theme")
@section("title")
	Registrar nuevo proveedor
@endsection

@section("content")
	<div class="contain-inner-section">
		<div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Nuevo proveedor</span></h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                <li class="text-info">Sales</li>
                 <li><a href="{{ url("dashboard/categories") }}">Providers</a></li>
                <li class="active">New</li>
            </ol>
        </div>

    	@if(count($errors)>0)
    		@include("layouts.dashboard.message"){{--message--}}
    	@endif
        <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
	                <div class="section-header">
	                	<h2>Registrar proveedor</h2>
	                </div>
	                <div class="section-body">
		                {!! Form::open(array("url"=>"dashboard/compras/personas/providers", "method"=>"POST", "autocomplete"=>"off")) !!}
		                {!! Form::token() !!}
			                @include("dashboard.compras.providers.partials.form-create")
		               {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- @section("scripts")
	<script src="{{ asset("assets/global/js/form_input.min.js") }}"></script>
@endsection --}}