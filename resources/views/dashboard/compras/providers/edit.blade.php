@extends("layouts.dashboard.theme")
@section("title")
	Edición de proveedor
@endsection

@section("styles")
	<style type="text/css">
		#textarea-cat{
			height: 10rem;
			max-width: 31.5rem;
			max-height: 31.5rem;
		}
	</style>
@endsection

@section("content")
	<div class="contain-inner-section">
		<div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Editar datos del proveedor</span></h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                <li class="text-info">Compras</li>
                 <li><a href="{{ url("dashboard/categories") }}">Providers</a></li>
                <li class="active">Edit</li>
            </ol>
        </div>

    	@if(count($errors)>0)
    		@include("layouts.dashboard.message"){{--message--}}
    	@endif
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
	                <div class="section-header">
	                	<h2>Editar datos del proveedor</h2>
	                </div>
	                <div class="section-body">
		                {!! Form::model($persona,["method" => "PATCH", "route" => ["providers.update", $persona->idpersona]]) !!}
		                {!! Form::token() !!}
			                @include("dashboard.compras.providers.partials.form-edit")
		               {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

