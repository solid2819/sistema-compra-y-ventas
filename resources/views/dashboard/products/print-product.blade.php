@section("title")
    {{ $product->name }}
@endsection
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="section-header">
                           <h1 class="page-name-title">Producto: {{ $product->name }}</span></h1>
                        </div>
                        <div class="section-body">

                            <div class="table-responsive">
                                <table class="table table-striped tab-content table-bordered" style="border: 2px solid #000">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>NOMBRE DEL PRODUCTO| </th>
                                        <th>CODIGO| </th>
                                        <th>CANTIDAD EN INVENTARIO| </th>

                                    </tr>
                                    </thead>
                                    <tbody style="border: 2px solid #000">
                                    <tr style="text-align: center; " >
                                        
                                        <td  >{{ $product->id }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->code }}</td>
                                        <td>{{ $product->stok }}</td>
                                    </tr>
                                    
                                    </tbody>
                                </table>
                            </div>

                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       

                            <div class="table-responsive" style="margin-top: 50px">
                                <table class="table table-striped tab-content table-bordered" style="border: 2px solid #000">
                                    <thead>
                                    <tr >
                                        <th colspan="4" >DESCRIPCION DEL PRODUCTO</th>
                                       

                                    </tr>
                                    </thead>
                                    <tbody style="">
                                    <tr style="text-align: center; " >
                                        
                                        
                                        <td>{!! $product->description !!}</td>
                                        
                                    </tr>
                                    
                                    </tbody>
                                </table>


                        </div>
                    </div>