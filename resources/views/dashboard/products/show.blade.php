@extends("layouts.dashboard.theme")
@section("title")
	{{ $product->name }}
@endsection

@section("styles")
	<style type="text/css">
		#textarea-cat{
			height: 10rem;
			max-width: 31.5rem;
			max-height: 31.5rem;
		}
	</style>
@endsection

@section("content")
	<div class="contain-inner-section">
		<div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Producto: {{ $product->name }}</span></h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                <li class="text-info">Almacen</li>
                 <li><a href="{{ url("dashboard/products") }}">Products</a></li>
                <li class="active">Edit</li>
            </ol>
        </div>

    	
        <div class="row">
           <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6">

           	<div class="section-header ">
	            <h2><b>Nombre del producto</b></h2>
	        </div>
           	<ul>
           		<li><h6>{{ $product->name }}</h6></li>
           	</ul>


           	{{----}}
           	<div class="section-header ">
	            <h2><b>Código del producto</b></h2>
	        </div>
           	<ul>
           		<li><h6>{{ $product->code }}</h6></li>
           	</ul>

           	{{----}}
           	<div class="section-header ">
	            <h2><b>Categoría</b></h2>
	        </div>
           	<ul>
           		<li><h6>{{ $product->category }}</h6></li>
           	</ul>

           	{{----}}
           	<div class="section-header ">
	            <h2><b>Actualmente en Stock</b></h2>
	        </div>
           	<ul>
           		<li><h6>{{ $product->stok }}</h6></li>
           	</ul>

              {{----}}
            <div class="section-header ">
              <h2><b>Descripción</b></h2>
          </div>
            <ul>
              <li><h6>{!! $product->description !!}</h6></li>
            </ul>

           	{{----}}
              <div class="section-header ">
              <h2><b> <a target="_blank" href="{{ route("downpdf", $product->idarticulo) }}"><button class="btn bt-outline btn-link">Descargar PDF</button></a></b></h2>
          </div>
            <ul>
             
            </ul>


           </div>

           <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">

           	{{----}}
           	<div class="section-header ">
	            <h2><b>Agregado al sistema el:</b></h2> <h6> {{ $product->created_at }}</h6>
	        </div>
           
           	{{----}}
           	<div class="section-header ">
	            <h2><b>Última actualización: </b></h2><h6> {{ $product->updated_at }}</h6>
	        </div>
           	


           	{{----}}
           	<div class="section-header ">
	            <h2><b>Imagen del producto</b></h2>
	        </div>
           	
           		@if($product->file)
			       <img src="{{ asset("images/products/" . $product->file) }}"  alt="{{ $product->name }}" class="img-thumbnail img-pro">
			    @else
			    	<i class="fa fa-image"></i>
			    @endif
           	
           </div>
        </div>
    </div>
@endsection

@section("scripts")
	<script src="{{ asset("assets/global/js/form_input.min.js") }}"></script>
@endsection