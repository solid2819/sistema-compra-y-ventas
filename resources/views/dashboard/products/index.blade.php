@extends("layouts.dashboard.theme")
@section("title")
	Productos
@endsection

@section("styles")
	<style type="text/css" media="screen">
		.btn1{
			width: 6rem!important;
		}
		.img-pro{
			width: 8vw!important;
			height: 8vw!important;
		}
	</style>
@endsection

@section("content")
	<div class="contain-inner-section">
		  <div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Productos</h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                <li class="text-info">Almacen</li>
                <li class="active">Productos</li>
            </ol>
        </div>
    	@include("layouts.dashboard.message"){{--message--}}
        <div class="row">
            <div class="">
	            <div class="section-header">
	                <a href="{{ url("dashboard/products/create") }}">
	                	<button type="button" class="btn btn-primary btn-outline float-button-light">
	                        <i class="fa fa-plus"> </i> Nuevo
	                 	</button>
	                </a>
	            </div>
	            @include("dashboard.products.partials.search")
	            <div class="section-body">
	                <div class="table-responsive">
	                   @if(count($products))
	                     <table class="table table-hover">
		                        <thead>
		                        <tr>
		                        	<th>#</th>
		                        	<th></th>
		                            <th>Producto</th>
		                              {{-- <th>Categoría</th> --}}
		                            <th>Código</th>
		                            <th>Stock</th>
		                            <th>Estado</th>
		                            {{-- <th>Fecha de creación</th>
		                            <th>Última actualización</th> --}}
		                        </tr>
		                        </thead>
		                        <tbody>
		                        @foreach($products as $product)
			                        <tr class="text-capitalize">
			                        	 <th scope="row">
			                            	{{ $product->idarticulo }}
			                            </th>
			                        	<td>
			                        		@if($product->file)
			                        		<img src="{{ asset("images/products/" . $product->file) }}"  alt="{{ $product->name }}" class="img-thumbnail img-pro">
			                        		@else
			                        		<i class="fa fa-image"></i>
			                        		@endif
			                        	</td>	
			                            <td>{{ $product->name }}
			                             <br>
			                             <br>
			                             <a href="{{ route("products.show" , $product->idarticulo) }}">
			                               <button type="button" class="btn1 btn btn-primary btn-outline btn-xs float-button-light">
			                                    <span class="fa fa-eye"> Ver</span>
			                               </button>
			                             </a>
			                             <a href="{{ route("products.edit" , $product->idarticulo) }}">
			                               <button type="button" class="btn1 btn btn-success btn-outline btn-xs float-button-light">
			                                    <span class="fa fa-exchange"></span> Modificar
			                               </button>
			                             </a>
			                             <a href="#">
			                              <button type="button" data-target="#modal-delete-{{ $product->idarticulo }}" data-toggle="modal" class="btn1 btn btn-danger btn-outline btn-xs float-button-light">
			                               		<span class="fa fa-trash"></span> Eliminar
			                              </button>
			                             </a>
			                            </td>
			                            {{--  <td>{{ $product->category }}</td> --}}
			                            <td>{{ $product->code }}</td>
			                            <td>{{ $product->stok }}</td>
			                            <td>{{ $product->status }}</td>
			                            {{-- <td>{{ $product->created_at }}</td>
			                            <td>{{ $product->updated_at }}</td> --}}
			                        </tr>
			                        @include("dashboard.products.partials.modal_delete")
		                        @endforeach
		                        </tbody>
		                  </table>
		                 {{--  <a href="{{ route("downpdfall") }}" target="_blank" ><button class="pull-right btn-link">Descargar en PDF</button></a> --}}
	                    @else
	                        No hay Productos   
	                       	@if($searchText)
	                       		<a href="{{ url("dashboard/products") }}">
			                       	 <button type="button" class="btn1 btn btn-info btn-outline btn-xs float-button-light" data-target="#danger-modal" data-toggle="modal">
					                    <span class="fa fa-arrow-left"></span> Regresar
					                </button>
	                       		</a>
	                       	@endif
	                    @endif
	                </div>
	                {{ $products->render() }}
	                
	            </div>
	        </div>
        </div>
    </div>
@endsection