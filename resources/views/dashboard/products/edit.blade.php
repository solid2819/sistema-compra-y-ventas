@extends("layouts.dashboard.theme")
@section("title")
	Edición de producto
@endsection

@section("styles")
	
@endsection

@section("content")
	<div class="contain-inner-section">
		<div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Editar Producto</span></h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                <li class="text-info">Almacen</li>
                 <li><a href="{{ url("dashboard/products") }}">Products</a></li>
                <li class="active">Edit</li>
            </ol>
        </div>

    	@if(count($errors)>0)
    		@include("layouts.dashboard.message"){{--message--}}
    	@endif
        <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
	                <div class="section-header">
	                	<h2>Edición del Producto <span class="text-capitalize"> {{ $product->name }}</span></h2> 
	                </div>
	                <div class="section-body">
		                {!! Form::model($product,["method" => "PATCH", "route" => ["products.update", $product->idarticulo], "files" => "true"]) !!}
		                {!! Form::token() !!}
			                @include("dashboard.products.partials.form-edit")
		               {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
	<script src="{{ asset("assets/global/js/form_input.min.js") }}"></script>
@endsection