@section("styles")
	<style type="text/css" media="screen">
		#stock_product{
			width: 200px!important;
		}	
	</style>
@endsection
<div class="row">
	{{-- {{ Form::hidden("user", auth()->user()->id) }} --}}
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> {{-- columna Título y slug de producto --}}
		<div class="form-item">
			<p {{-- class="formLabel" --}}>Título del producto</p>
			{{ Form::text("name",null, ["class" => "form-style", "id" => "name_pro", "autocomplete" => "off"])}} 
		</div>

		

		<div class="form-item">
			<p {{-- class="formLabel" --}}>Código del producto</p>
			{{ Form::text("code",null, ["class" => "form-style", "id" => "code", "autocomplete" => "off"])}} 
		</div>
	</div>{{-- end columna Título y slug de producto --}}

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

		{{-- Categoría del producto --}}
		<div class="form-item" style="margin-top:20px">
			<p {{-- class="formLabel" --}}>  Elige una categoría para el producto</p>
		    {{ Form::select("idcategoria", $categories, null , ["class" => "form-style", "style" =>"margin-top:-10px"])}}
		</div>

		{{-- Stock del producto --}}
		<div class="form-item">
			<p {{-- class="formLabel" --}}>¿Cuántos hay en Stock?</p>
			<input class="form-style" name="stok" id="stock_product" autocomplete="off" type="number">
			{{-- {{ Form::number("stok",null, ["class" => "form-style", "id" => "stock_product", "autocomplete" => "off"])}}  --}}
		</div>

		{{-- Imagen del producto --}}
		<div class="form-item">
			<p {{-- class="formLabel" --}}>Imagen del producto</p>
			{{ Form::file("file", ["class" => "form-style", "id" => "file", "autocomplete" => "off"])}} 
		</div>
	</div>
</div>

<div class="form-item">
	<p {{-- class="formLabel" --}}>Descripción</p>
	{{ Form::textarea("description",null, ["class" => "form-style", "id" => "textarea_pro", "autocomplete" => "off"])}} 
		                        	
	</textarea>
</div>

<button type="submit" class="btn btn-primary btn-outline float-button-light waves-effect waves-button waves-float waves-light">
	Crear
</button>
<button type="reset" class="btn btn-inverse btn-outline float-button-light waves-effect waves-button waves-float waves-light">
	Borrar
</button>
<a href="{{ url("dashboard/products") }}">
	<button type="button" class="btn btn-warning btn-outline float-button-light waves-effect waves-button waves-float waves-light">
		Volver
	</button>
</a>

@section("scripts")
  <script src="{{ asset("vendor/jquery.stringToSlug/jquery.stringToSlug.min.js") }} "></script>
  <script src="{{ asset("vendor/ckeditor/ckeditor.js") }} "></script>
  
  <script>
    $(document).ready(function () {
      $("#name_pro, #slug_pro").stringToSlug({
        callback: function (text) {
          $("#slug_pro").val(text);
        }
      });
    });

    /*CK EDITOR*/
	  CKEDITOR.config.height=400;
	  CKEDITOR.config.width="auto";

	  CKEDITOR.replace("textarea_pro");
  </script>
@endsection