@section("title")
    Todos los productos
@endsection




<div class="section-body">
                    <div class="table-responsive">
                       
                         <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th></th>
                                    <th>Producto</th>
                                      <th>Categoría</th>
                                    <th>Código</th>
                                    <th>Stock</th>
                                    <th>Estado</th>
                                    {{-- <th>Fecha de creación</th>
                                    <th>Última actualización</th> --}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr class="text-capitalize">
                                         <th scope="row">
                                            {{ $product->id }}
                                        </th>
                                        <td>
                                            @if($product->file)
                                            <img src="{{ asset("images/products/" . $product->file) }}"  alt="{{ $product->name }}" class="img-thumbnail img-pro">
                                            @else
                                            <i class="fa fa-image"></i>
                                            @endif
                                        </td>   
                                        <td>{{ $product->name }}
                                         
                                        </td>
                                         <td>{{ $product->category }}</td>
                                        <td>{{ $product->code }}</td>
                                        <td>{{ $product->stok }}</td>
                                        <td>{{ $product->status }}</td>
                                        {{-- <td>{{ $product->created_at }}</td>
                                        <td>{{ $product->updated_at }}</td> --}}
                                    </tr>
                                    @include("dashboard.products.partials.modal_delete")
                                @endforeach
                                </tbody>
                          </table>
                        
                    </div>
                   
                </div>

