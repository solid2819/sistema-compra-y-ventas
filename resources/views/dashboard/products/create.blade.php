@extends("layouts.dashboard.theme")
@section("title")
	Nuevo Producto
@endsection

@section("styles")
@endsection

@section("content")
	<div class="contain-inner-section">
		<div class="page-main-header">
            <!-- Page Title -->
            <h1 class="page-name-title">Nuevo Producto</span></h1>
            <!--  Breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="{{ route("dashboard") }}">Dashboard</a></li>
                <li class="text-info">Almacen</li>
                 <li><a href="{{ url("dashboard/products") }}">Products</a></li>
                <li class="active">New</li>
            </ol>
        </div>

    	@if(count($errors)>0)
    		@include("layouts.dashboard.message"){{--message--}}
    	@endif
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
	                <div class="section-header">
	                	<h2>Crear Producto</h2>
	                </div>
	                <div class="section-body">
		                {!! Form::open(array("url"=>"dashboard/products", "method"=>"POST", "autocomplete"=>"off", "files" => "true")) !!}
		                {!! Form::token() !!}
			                @include("dashboard.products.partials.form-create")
		               {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection