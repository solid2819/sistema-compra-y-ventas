<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*Dashboard*/
Route::get("/", function (){
	
	return view("auth.login");

});


Route::group(["middleware" => ["auth"]], function(){

	Route::get("/dashboard", "Admin\AdminController@dashboard")->name("dashboard");

	Route::resource("/dashboard/categories", "Admin\CategoryController");
	Route::resource("/dashboard/products", "Admin\ProductController");

	Route::get("products/pdf/", "Admin\ProductController@downloadAllPDF")->name("downpdfall");
	Route::get("product/pdf/{id}", "Admin\ProductController@downloadPDF")->name("downpdf");

	Route::resource("/dashboard/ventas/personas", "Admin\ClientController");
	Route::resource("/dashboard/ventas/ventas-clientes/", "Admin\VentaController");

	Route::resource("/dashboard/compras/personas/providers", "Admin\ProviderController");
	Route::resource("/dashboard/compras/ingresos", "Admin\IngresoController");
		Route::get("compra/pdf/{id}", "Admin\IngresoController@downloadPDF")->name("fac-compra");


	Route::resource("/dashboard/users", "Admin\UsersController");
	
	// Route::get('/home', 'HomeController@index')->name('home');

});

Auth::routes();