<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    
	$title=$faker->sentence(4);
    $stok=100;
    return [
        //

    	// "user_id" => rand(1,30),
    	"idcategoria" => rand(1,20),
    	"code" => $faker->text(5),
        "name" => $title,
       
        // "excerpt" => $faker->text(100),
        "description" => $faker->text(1000),
        "file" => $faker->imageUrl($width = 1200 , $height = 400),
        "stok" => $stok,
        "status" => $faker->randomElement(["Publicado", "Reciclado"]), 
    ];
});