<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('idarticulo');
            $table->integer("idcategoria")->unsigned();
            $table->string("code",50);
            $table->string("name", 100);
            
            $table->longtext("description")->nulleable();
            $table->string("file",255)->nulleable();
            $table->integer("stok")->nulleable();
             $table->enum('status', ["Publicado", "Reciclado"])->default("Publicado");
            $table->timestamps();

            /*Relations*/
            $table->foreign('idcategoria')->references('idcategoria')->on('categories')
             ->onDelete('cascade')
             ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
