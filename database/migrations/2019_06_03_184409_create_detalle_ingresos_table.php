<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_ingresos', function (Blueprint $table) {
            $table->bigIncrements('iddetalle_ingreso');

            $table->integer("idingreso")->unsigned();
            $table->integer("idarticulo")->unsigned();

            $table->integer("cantidad");
            $table->decimal("precio_compra", 11,2);
            $table->decimal("precio_venta",  11,2);
            $table->timestamps();


            /*Relations*/

             $table->foreign('idingreso')->references('idingreso')->on('ingresos')
             ->onDelete('cascade')
             ->onUpdate('cascade');

             $table->foreign('idarticulo')->references('idarticulo')->on('products')
             ->onDelete('cascade')
             ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_ingresos');
    }
}
