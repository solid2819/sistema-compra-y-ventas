<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_ventas', function (Blueprint $table) {
            $table->bigIncrements('iddetalle_venta');
            $table->integer("idventa")->unsigned();
            $table->integer("idarticulo")->unsigned();
            $table->integer("cantidad");
            $table->decimal("precio_venta", 11,2);
            $table->decimal("descuento", 11,2);

            $table->timestamps();

             $table->foreign('idventa')->references('idventa')->on('ventas')
             ->onDelete('cascade')
             ->onUpdate('cascade');

              $table->foreign('idarticulo')->references('idarticulo')->on('products')
             ->onDelete('cascade')
             ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalles_ventas');
    }
}
