<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
            "idcategoria" => "required",
            "code"        => "required|max:50",
            "name"        => "required|max:50",
            
            "stok"        => "required|numeric",
            "file"        => "mimes:jpeg,jpg,bmp,png",
        ];
    }
}
