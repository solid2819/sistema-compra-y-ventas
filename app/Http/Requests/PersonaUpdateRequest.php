<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonaUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             
            "name"          => "required|max:100",
            // "slug"          => "required|unique:personas,slug," . $this->persona,
            "document"      => "required|max:20",
            "nro_document"  => "required|max:15",
            "address"       => "max:200",
            "fhone"         => "max:15",
            "email"         => "max:70"
        ];
    }
}
