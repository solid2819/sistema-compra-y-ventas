<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VentaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
                
            "idcliente"    =>  "required",
            "tipo_voucher"    =>  "required|max:30",
            "serie_voucher"    =>  "max:10",
            "nro_voucher"    =>  "required",

            //validate detalle_ingresos

            "idarticulo"    =>  "required",
            "cantidad"  =>  "required",
            "precio_venta"    =>  "required"
            "descuento"    =>  "required",
            "total_venta"    =>  "required",

        ];
    }
}
