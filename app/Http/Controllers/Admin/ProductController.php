<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Product;
use PDF;

use App\Category;
use DB;


class ProductController extends Controller
{
    
   public function __construct()
    {
        $this->middleware("auth");
    }

    public function index(Request $request)
    {
        if($request){
            $query=trim($request->get('searchText'));//filtro de busqueda
            // $products=DB::table('products as a')
            //     ->join("categories as c", "a.idarticulo", "=", "c.idcategoria")
            //     ->select("a.idarticulo","a.name", "a.code", "a.stok", "c.name as category", "a.description", "a.file", "a.status")
            //     ->where('a.name', 'LIKE', '%' .$query. '%')
            //     ->orwhere('a.code', 'LIKE', '%' .$query. '%')
            //     ->orwhere('a.idcategoria', 'LIKE', '%' .$query. '%')
            //     // ->orwhere('a.status', 'LIKE', '%' .$query. '%')
            //     // ->orWhere('description', 'LIKE', '%' .$query. '%')
            //     ->orderBy('a.idarticulo', 'desc')
            //     ->paginate(10);
            $products=DB::table('products')
                ->where('name', 'LIKE', '%' .$query. '%')
                ->orWhere('code', 'LIKE', '%' .$query. '%')
                ->where('status', '=' , 'Publicado')
                
                ->paginate(10);


            return view("dashboard.products.index", ["products" => $products, "searchText"=>$query]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy("name", "ASC")->pluck("name", "idcategoria");

        // $categories =  DB::table("categories")->where("status", "=", "Activa")->get();

        return view("dashboard.products.create", compact("categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        $product = new Product;
        $product->idcategoria=$request->get('idcategoria');
        $product->code=$request->get('code');
        $product->name=$request->get('name');
        
        $product->description=$request->get("description");

        $product->stok=$request->get('stok');
        $product->status="Publicado";

        if (Input::hasFile("file")) {
            $file = Input::file("file");
            $file->move(public_path() . "/images/products/", $file->getClientOriginalName());

            $product->file=$file->getClientOriginalName();
        }
        $product->save();

        return Redirect::to("dashboard/products")->with("info", "Se ha agregado un nuevo Producto '" . $product->name . "'");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $categories = Category::orderBy("name", "ASC")->pluck("name", "idcategoria");

        return view("dashboard.products.show", ['product'=>Product::findOrFail($id)], compact("categories"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        // $categories = DB::table("categories")->where("status", "=" , "Activa");
        $categories = Category::orderBy("name", "ASC")->pluck("name", "idcategoria");

        return view("dashboard.products.edit", compact("product", "categories"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        $product=Product::findOrFail($id);
        $product->idcategoria=$request->get('idcategoria');
        $product->code=$request->get("code");
        $product->name=$request->get("name");
       
        $product->description=$request->get("description");

        $product->stok=$request->get('stok');
        $product->status="Publicado";

        if (Input::hasFile("file")) {
            $file = Input::file("file");
            $file->move(public_path() . "/images/products/", $file->getClientOriginalName());

            $product->file=$file->getClientOriginalName();
        }

        $product->update();

        return Redirect::to("dashboard/products")->with("info", "Producto actualizadacon éxito");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $product=Product::findOrFail($id);
        // $product->status="DRAFT";
        // $product->update();

        // return Redirect::to("dashboard/products")->with("info", "Se ha cambiado el Status del producto a Borrador");

        $product = Product::find($id)->delete();

        return back()->with("info", "El Producto ha sido eliminado" );
    }

    public function downloadAllPDF()
    {
       
       $products = Product::orderBy("idarticulo", "DESC");
        $pdf = PDF::loadView("dashboard.products.print-products", compact("products")); 

        return $pdf->stream();
       
    }

    public function downloadPDF($id)
    {
       
        $product = Product::findOrFail($id);
        $pdf = PDF::loadView("dashboard.products.print-product", compact("product")); 

        return $pdf->stream();
       
    }


}
