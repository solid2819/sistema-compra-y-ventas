<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\PersonaUpdateRequest;
use App\Http\Requests\PersonaStoreRequest;
use App\Persona;
use DB;

class ProviderController extends Controller
{
    
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function index(Request $request)
    {
        if($request){
            $query=trim($request->get('searchText'));//filtro de busqueda
            $personas=DB::table('personas')
                ->where('name', 'LIKE', '%' .$query. '%')
                ->orWhere('document', 'LIKE', '%' .$query. '%')
                // ->orWhere('description', 'LIKE', '%' .$query. '%')
                ->where('tipo_cliente', '=' , 'Proveedor')
                ->orWhere("nro_document", 'LIKE', '%' .$query. '%')
                ->where('tipo_cliente', '=' , 'Proveedor')
                ->orderBy('idpersona', 'desc')
                ->paginate(10);

            return view("dashboard.compras.providers.index", ["personas" => $personas, "searchText"=>$query]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("dashboard.compras.providers.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonaStoreRequest $request)
    {
        $persona = new Persona;
        $persona->tipo_cliente="Proveedor";
        $persona->name=$request->get('name');
        // $persona->slug=$request->get('slug');
        $persona->email=$request->get('email');
        $persona->document=$request->get('document');
        $persona->nro_document=$request->get('nro_document');
        $persona->phone=$request->get('phone');
        $persona->address=$request->get('address');
       
        $persona->save();

        return Redirect::to("dashboard/compras/personas/providers")->with("info", "Se ha registrado exitosamente a:  '" . $persona->name . "'");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view("dashboard.compras.providers.show", ['persona'=>Persona::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view("dashboard.compras.providers.edit", ['persona'=>Persona::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PersonaUpdateRequest $request, $id)
    {
        $persona=Persona::findOrFail($id);
        $persona->name=$request->get('name');
        // $persona->slug=$request->get('slug');
        $persona->email=$request->get('email');
        $persona->document=$request->get('document');
        $persona->nro_document=$request->get('nro_document');
        $persona->phone=$request->get('phone');
        $persona->address=$request->get('address');

        $persona->update();

        return Redirect::to("dashboard/compras/personas/providers")->with("info", "Actualizado con éxito");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $persona=Persona::findOrFail($id);
        // $persona->status="Inactivo";
        // $persona->update();

        // return Redirect::to("dashboard/compras/personas/providers")->with("info", "El Proveedor se ha puesto en estado inactivo");

        $persona = Persona::find($id)->delete();

        return back()->with("info", "Proveedor ha sido añadido a la lista de INACTIVOS" );
    }
}
