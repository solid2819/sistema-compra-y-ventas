<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\IngresoRequest;

use App\Ingreso;
use App\Persona;
use App\DetalleIngreso;
use DB;
use PDF;

use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;

class IngresoController extends Controller
{
   public function __construct()
    {
        $this->middleware("auth");
    }

    public function index(Request $request)
    {
        if($request){

        	$query=trim($request->get("searchText"));//trim:elimina los espacios al comienzo y final
        	$ingresos=DB::table("ingresos as i")
        	->join("personas as p", "i.idproveedor", "=", "p.idpersona")
        	->join("detalle_ingresos as di", "i.idingreso", "=", "di.idingreso")
        	->select("i.idingreso", "i.date", "p.name", "i.tipo_voucher", "i.serie_voucher", "i.nro_voucher", "i.impuesto", "i.status", DB::raw('sum(di.cantidad*precio_compra) as total'))
        	->where("i.nro_voucher", "LIKE", "%" . $query . "%")
        	->orWhere("i.serie_voucher", "LIKE", "%" . $query . "%")
        	->orderBy("i.idingreso","desc")
        	->groupBy("i.idingreso", "i.date", "p.name", "i.tipo_voucher", "i.serie_voucher", "i.nro_voucher", "i.impuesto", "i.status")
        	->paginate(10);

        	return view("dashboard.compras.ingresos.index",["ingresos" => $ingresos, "searchText" => $query]);
        }
    }

    public function create()
    {
    	$proveedores=Persona::orderBy("name", "ASC")->pluck("name", "idpersona");
    	$persona=DB::table("personas")->where("tipo_cliente", "=", "Proveedor")->get();
    	$articulos=DB::table("products as art")
    		->select(DB::raw('CONCAT(art.code, " - ",art.name) AS articulo'), "art.idarticulo")
    		->where("art.status", "=", "Publicado")
    		->get();

    		return view("dashboard.compras.ingresos.create", compact("proveedores", "persona", "articulos"));
    }


    public function store(IngresoRequest $request)
    {
    	//Capturar exepciones en caso de error. Al momento de registrar un ingreso, se debe almacenar tambien el detalle de ingreso. Deben almacernarce ambos
    	try{
    		DB::beginTransaction();

    		//Guardar ingreso
    		$ingreso=new Ingreso();
    		$ingreso->idproveedor=$request->get("idproveedor");
    		$ingreso->tipo_voucher=$request->get("tipo_voucher");
    		$ingreso->serie_voucher=$request->get("serie_voucher");
    		$ingreso->nro_voucher=$request->get("nro_voucher");
    		$mytime= Carbon::now("America/Caracas");
    		$ingreso->date=$mytime->toDateTimeString();
    		$ingreso->impuesto="16";
    		$ingreso->status="Aprovado";

    		$ingreso->save();

    		//Guardar detalle de ingreso

    		$idarticulo=$request->get("idarticulo" );
   			$cantidad=$request->get("cantidad");
   			$precio_compra=$request->get("precio_compra");
   			$precio_venta=$request->get("precio_venta");

   			$cont = 0;

   			while($cont < count($idarticulo)) {

   				$detalle=new DetalleIngreso();
   				$detalle->idingreso=$ingreso->idingreso;//Verificar id
   				$detalle->idarticulo=$idarticulo[$cont];
   				$detalle->cantidad=$cantidad[$cont];
   				$detalle->precio_compra=$precio_compra[$cont];
   				$detalle->precio_venta=$precio_venta[$cont];
   				$detalle->save();

   				$cont=$cont+1;		
   			}	

    		DB::commit();
    	 }
    	catch(\Execption $e){
    		DB::rollback();
    	}

    	return Redirect::to("dashboard/compras/ingresos");
    }

    public function show($id)
    {

       $ingreso=DB::table("ingresos as i")
            ->join("personas as p", "i.idproveedor", "=", "p.idpersona")
            ->join("detalle_ingresos as di", "i.idingreso", "=", "di.idingreso")
            ->select("i.idingreso", "i.date", "p.name", "i.tipo_voucher", "i.serie_voucher", "i.nro_voucher", "i.impuesto", "i.status", DB::raw('sum(di.cantidad*precio_compra) as total'))
            ->where("i.idingreso", "=", $id)
            ->groupBy("i.idingreso", "i.date", "p.name", "i.tipo_voucher", "i.serie_voucher", "i.nro_voucher", "i.impuesto", "i.status")
            ->first();

        $detalles=DB::table("detalle_ingresos as d")
            ->join("products as a", "d.idarticulo", "=", "a.idarticulo")
            ->select("a.name as articulo", "d.cantidad", "d.precio_compra", "d.precio_venta")
            ->where("d.idingreso", "=", $id)
            ->get();    

    	
        return view("dashboard.compras.ingresos.show",compact("ingreso","detalles"));
    }


    public function destroy($id)
    {
    	$ingreso=Ingreso::findOrFail($id);
    	$ingreso->status="Cancelado";
    	$ingreso->update();

    	return Redirect::to("dashboard/compras/ingresos");



        // $category = Category::find($id)->delete();

        // return back()->with("info", "La Categoría ha sido eliminada" );
    }

    public function downloadPDF($id)
    {
        // $ingreso = Ingreso::findOrFail();
        $ingreso=DB::table("ingresos as i")
            ->join("personas as p", "i.idproveedor", "=", "p.idpersona")
            ->join("detalle_ingresos as di", "i.idingreso", "=", "di.idingreso")
            ->select("i.idingreso", "i.date", "p.name", "i.tipo_voucher", "i.serie_voucher", "i.nro_voucher", "i.impuesto", "i.status", DB::raw('sum(di.cantidad*precio_compra) as total'))
            ->where("i.idingreso", "=", $id)
            ->groupBy("i.idingreso", "i.date", "p.name", "i.tipo_voucher", "i.serie_voucher", "i.nro_voucher", "i.impuesto", "i.status")
            ->first();

        $detalles=DB::table("detalle_ingresos as d")
            ->join("products as a", "d.idarticulo", "=", "a.idarticulo")
            ->select("a.name as articulo", "d.cantidad", "d.precio_compra", "d.precio_venta")
            ->where("d.idingreso", "=", $id)
            ->get();  
        $compra = Ingreso::findOrFail($id);
        $pdf = PDF::loadView("dashboard.compras.ingresos.print-ingreso", compact("compra", "ingreso", "detalles")); 

        return $pdf->stream();

    }

}
