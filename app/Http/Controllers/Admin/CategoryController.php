<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Category;
use DB;

class CategoryController extends Controller
{
    
   public function __construct()
    {
        $this->middleware("auth");
    }

    public function index(Request $request)
    {
        if($request){
            $query=trim($request->get('searchText'));//filtro de busqueda
            $categories=DB::table('categories')
                ->where('name', 'LIKE', '%' .$query. '%')
                // ->orWhere('description', 'LIKE', '%' .$query. '%')
                ->where('status', '=' , 'Activa')
                ->orderBy('idcategoria', 'desc')
                ->paginate(10);

            return view("dashboard.categories.index", ["categories" => $categories, "searchText"=>$query]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("dashboard.categories.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        $category = new Category;
        $category->name=$request->get('name');
        // $category->slug=$request->get('slug');
        $category->description=$request->get("description");
        // $category->status="Activa";
        $category->save();

        return Redirect::to("dashboard/categories")->with("info", "Se ha agregado una nueva categoría '" . $category->name . "'");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view("dashboard.categories.show", ['category'=>Category::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view("dashboard.categories.edit", ['category'=>Category::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, $id)
    {
        $category=Category::findOrFail($id);
        $category->name=$request->get("name");
        // $category->slug=$request->get("slug");
        $category->description=$request->get("description");
        $category->update();

        return Redirect::to("dashboard/categories")->with("info", "Categoría actualizadacon éxito");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=Category::findOrFail($id);
        $category->status="Desactivada";
        $category->update();

        return Redirect::to("dashboard/categories")->with("info", "La categoría se ha eliminado");

        // $category = Category::find($id)->delete();

        // return back()->with("info", "La Categoría ha sido eliminada" );
    }
}
