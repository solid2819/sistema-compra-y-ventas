<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\UserFormRequest;

use App\User; 

use DB;

class UsersController extends Controller
{
    
   public function __construct()
    {
        $this->middleware("auth");
    }

    public function index(Request $request)
    {
        if($request){
            $query=trim($request->get('searchText'));//filtro de busqueda
            $users=DB::table('users')
                ->where('name', 'LIKE', '%' .$query. '%')
                // ->orWhere('description', 'LIKE', '%' .$query. '%')
                ->orWhere('email', 'LIKE', '%' .$query. '%')
                ->orderBy('id', 'desc')
                ->paginate(10);

            return view("dashboard.users.index", ["users" => $users, "searchText"=>$query]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("dashboard.users.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFormRequest $request)
    {
        $user = new User;
        $user->name=$request->get('name');
       
        $user->email=$request->get("email");
        $user->password=bcrypt($request->get("password"));
        $user->save();

        return Redirect::to("dashboard/users")->with("info", "Se ha agregado un nuevo usuario '" . $user->name . "'");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view("dashboard.users.show", ['user'=>User::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view("dashboard.users.edit", ['user'=>User::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserFormRequest $request, $id)
    {
        $user=User::findOrFail($id);
        $user->name=$request->get("name");
        $user->email=$request->get("email");
        $user->password=bcrypt($request->get("password"));
        $user->update();

        return Redirect::to("dashboard/users")->with("info", "Usuario actualizado con éxito");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $category=Category::findOrFail($id);
        // $category->status="Desactivada";
        // $category->update();

        // return Redirect::to("dashboard/categories")->with("info", "La categoría se ha eliminado");

        $user = User::find($id)->delete();

        return back()->with("info", "Usuario eliminado del sistema" );
    }
}

