<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\VentaFormRequest;
use App\Http\Requests\IngresoRequest;

use App\Venta;
use App\Persona;

use App\DetalleVenta;
use DB;

use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;

class VentaController extends Controller{

    public function __construct()
    {
        $this->middleware("auth");
    }

    public function index(Request $request)
    {
        if($request){

            $query=trim($request->get("searchText"));//trim:elimina los espacios al comienzo y final
            $ventas=DB::table("ventas as v")
            ->join("personas as p", "v.idcliente", "=", "p.idpersona")
            ->join("detalles_ventas as dv", "v.idventa", "=", "dv.idventa")
            ->select("v.idventa", "v.fecha_hora", "p.name", "v.tipo_voucher", "v.serie_voucher", "v.nro_voucher", "v.impuesto", "v.status", "v.total_venta")
            ->where("v.nro_voucher", "LIKE", "%" . $query . "%")
            ->orWhere("v.serie_voucher", "LIKE", "%" . $query . "%")
            ->orderBy("v.idventa","desc")
            ->groupBy("v.idventa", "v.fecha_hora", "p.name", "v.tipo_voucher", "v.serie_voucher", "v.nro_voucher", "v.impuesto", "v.status")
            ->paginate(10);

            return view("dashboard.ventas.venta.index",["ventas" => $ventas, "searchText" => $query]);
        }
    }

    public function create()
    {
        $clientes=Persona::orderBy("name", "ASC")->pluck("name", "idpersona");
        // $personas=DB::table("personas")->where("tipo_cliente", "=", "Cliente")->get();
        $articulos=DB::table("products as art")
            ->join("detalle_ingresos as di", "art.idarticulo", "=", "di.idarticulo")
            ->select(DB::raw('CONCAT(art.code, " - ",art.name) AS articulo'), "art.idarticulo", "art.stok", DB::raw('avg(di.precio_venta) as precio_promedio'))
            ->where("art.status", "=", "Publicado")
            ->where("art.stok", ">", "0")
            ->groupBy("articulo", "art.idarticulo", "art.stok")
            ->get();

        // $clientes=Persona::orderBy("name", "ASC")->pluck("name", "id");
        // $persona=DB::table("personas")->where("tipo_cliente", "=", "Cliente")->get();
        // $articulos=DB::table("products as art")
        //     ->select(DB::raw('CONCAT(art.code, " - ",art.name) AS products'), "art.id")
        //     ->where("art.status", "=", "Publicado")
        //     ->get();

            return view("dashboard.ventas.venta.create", compact("clientes", "personas", "articulos"));
    }


    public function store(VentaFormRequest $request)
    {
        //Capturar exepciones en caso de error. Al momento de registrar un ingreso, se debe almacenar tambien el detalle de ingreso. Deben almacernarce ambos
        try{
            DB::beginTransaction();

            //Guardar ingreso
            $venta=new Venta();
            $venta->idcliente=$request->get("idcliente");
            $venta->tipo_voucher=$request->get("tipo_voucher");
            $venta->serie_voucher=$request->get("serie_voucher");
            $venta->nro_voucher=$request->get("nro_voucher");
            $venta->total_venta=$request->get("total_venta");

            $mytime= Carbon::now("America/Caracas");
            $venta->date=$mytime->toDateTimeString();
            $venta->impuesto="16";
            $venta->status="Entregado";

            $venta->save();

            //Guardar detalle de ingreso

            $idarticulo=$request->get("idarticulo");
            $cantidad=$request->get("cantidad");
            $descuento=$request->get("descuento");
            $precio_venta=$request->get("precio_venta");

            $cont = 0;

            while ($cont < count($idarticulo)) {

                $detalle=new DetalleVenta();
                $detalle->idventa=$venta->idventa;//Verificar id
                $detalle->idarticulo=$idarticulo[$cont];
                $detalle->cantidad=$cantidad[$cont];
                $detalle->descuento=$descuento[$cont];
                $detalle->precio_venta=$precio_venta[$cont];
                $detalle->save();

                $cont=$cont+1;        
            }   

            DB::commit();
        }
        catch(\Execption $e){
            DB::rollback();
        }

        return Redirect::to("dashboard/ventas/ventas-clientes");
    }

    public function show($id)
    {
        $venta = DB::table("ventas as v")
            ->join("personas as p", "v.idcliente", "=", "p.idpersona")
            ->join("detalles_ventas as dv", "v.idventa", "=", "dv.idventa")
            ->select("v.idventa", "v.date", "p.name", "v.tipo_voucher", "v.serie_voucher", "v.nro_voucher", "v.impuesto", "v.status", "v.total_venta")
            ->where("v.idventa", "=", $id)
            ->first();

        $detalles=DB::table("detalles_ventas as d")
            ->join("products as a", "d.idarticulo", "=", "a.idarticulo")
            ->select("a.name as articulo", "d.cantidad", "d.descuento", "d.precio_venta")
            ->where("d.idventa", "=", $id)
            ->get();    

        return view("dashboard.ventas.venta.show",compact("venta","detalles"));
    }


    public function destroy($id)
    {
        $venta=Ventas::findOrFail($id);
        $venta->status="Cancelado";
        $venta->update();

        return Redirect::to("dashboard/ventas/ventas-clientes");



        // $category = Category::find($id)->delete();

        // return back()->with("info", "La Categoría ha sido eliminada" );
    }
}
