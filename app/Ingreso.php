<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    protected $table="ingresos";

    protected $primaryKey="idingreso";


    protected $filleable = [
    	"idproveedor", "tipo_voucher", "serie_voucher", "nro_voucher", "impuesto", "date", "status"
    ];

    protected $guarded=[

    ];
}
