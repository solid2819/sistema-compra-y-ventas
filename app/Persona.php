<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table="personas";

     protected $primaryKey="idpersona";


    protected $filleable = [
    	"tipo_cliente", "name", "slug", "email", "document", "nro_document", "phone", "address"
    ];

    protected $guarded=[

    ];
}
