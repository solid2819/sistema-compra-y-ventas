<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleIngreso extends Model
{
   protected $table="detalle_ingresos";

    protected $primaryKey="iddetalle_ingreso";


    protected $filleable = [
    	"idingreso", "idarticulo", "cantidad", "precio_compra", "precio_venta"
    ];

    protected $guarded=[
    	
    ];
}
