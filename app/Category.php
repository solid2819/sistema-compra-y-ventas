<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table="categories";

     protected $primaryKey="idcategoria";


    protected $filleable = [
    	"name", "slug", "description", "status"
    ];

    protected $guarded=[

    ];

}
