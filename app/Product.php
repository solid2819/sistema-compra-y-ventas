<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table="products";

     protected $primaryKey="idarticulo";


    protected $filleable = [
    	"idcategoria", "code", "name",  "description", "file", "stok", "status"
    ];

    protected $guarded=[

    ];
}
