<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $table="ventas";

    protected $primaryKey="idventa";


    protected $filleable = [
    	"idcliente", "tipo_voucher", "serie_voucher", "nro_voucher", "impuesto", "total_venta", "date", "status"
    ];

    protected $guarded=[

    ];
}
