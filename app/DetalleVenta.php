<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleVenta extends Model
{
    protected $table="detalles_ventas";

    protected $primaryKey="iddetalle_venta";


    protected $filleable = [
    	"idventa", "idarticulo", "cantidad", "precio_venta", "descuento"
    ];

    protected $guarded=[
    	
    ];
}
