<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesVentasCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_ventas_create', function (Blueprint $table) {
            $table->bigIncrements('iddetalle_venta');
            $table->integer("idventa")->unsigned();
            $table->integer("idarticulo")->unsigned();
            $table->integer("cantidad");
            $table->decimal("precio_venta", 11,2);
            $table->decimal("descuento", 11,2);

            $table->timestamps();

            $table->foreign('idarticulo')->references('id')->on('products')
             ->onDelete('cascade')
             ->onUpdate('cascade');
            
             $table->foreign('idventa')->references('id')->on('ventas')
             ->onDelete('cascade')
             ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalles_ventas_create');
    }
}
