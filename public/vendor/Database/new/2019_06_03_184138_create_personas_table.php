<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('idpersona');
            $table->string("tipo_cliente", 100);
            $table->string("name",128);
           
            $table->string("email", 128)->unique();
            $table->string("document", 40);
            $table->integer("nro_document")->unique();
            $table->integer("phone")->nullable();
            $table->string("address", 255)->nullable();
            $table->enum('status', ["ACTIVO", "INACTIVO"])->default("ACTIVO");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
