<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingresos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("provider_id")->unsigned();
            $table->string("tipo_voucher", 40);
            $table->string("serie_voucher", 40)->nullable();
            $table->string("nro_voucher", 40);
            $table->decimal("impuesto", 4,2);
            $table->datetime("date");
            $table->enum('status', ["Aprovado", "Cancelado"])->default("Aprovado");

            $table->timestamps();

           /*Relations*/
            $table->foreign('provider_id')->references('id')->on('personas')
             ->onDelete('cascade')
             ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingresos');
    }
}
