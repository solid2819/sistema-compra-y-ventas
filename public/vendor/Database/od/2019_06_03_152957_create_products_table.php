<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("category_id")->unsigned();
            $table->string("code",50);
            $table->string("name", 100);
            $table->string("slug",200)->unique();
            $table->longtext("description")->nulleable();
            $table->string("file",255)->nulleable();
            $table->integer("stok")->nulleable();
             $table->enum('status', ["Publicado", "Reciclado"])->default("Publicado");
            $table->timestamps();

            /*Relations*/
            $table->foreign('category_id')->references('id')->on('categories')
             ->onDelete('cascade')
             ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
