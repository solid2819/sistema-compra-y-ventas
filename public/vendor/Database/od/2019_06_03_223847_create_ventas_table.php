<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("cliente_id")->unsigned();
            $table->string("tipo_voucher", 40);
             $table->string("serie_voucher", 40);
            $table->string("nro_voucher", 40);
            $table->datetime("fecha_hora");
            $table->decimal("impuesto", 4,2);
            $table->decimal("total_venta", 11,2);
            $table->enum('status', ["Entregado", "Cancelada"])->default("Entregado");
            $table->timestamps();

            $table->foreign('cliente_id')->references('id')->on('personas')
             ->onDelete('cascade')
             ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
