<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_ventas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("ventas_id")->unsigned();
            $table->integer("product_id")->unsigned();
            $table->integer("cantidad");
            $table->decimal("precio_venta", 11,2);
            $table->decimal("descuento", 11,2);

            $table->timestamps();

             $table->foreign('ventas_id')->references('id')->on('ventas')
             ->onDelete('cascade')
             ->onUpdate('cascade');

              $table->foreign('product_id')->references('id')->on('products')
             ->onDelete('cascade')
             ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalles_ventas');
    }
}
