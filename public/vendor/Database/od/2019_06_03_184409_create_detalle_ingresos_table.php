<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_ingresos', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer("ingreso_id")->unsigned();
            $table->integer("producto_id")->unsigned();

            $table->integer("cantidad");
            $table->decimal("precio_compra", 11,2);
            $table->decimal("precio_venta",  11,2);
            $table->timestamps();


            /*Relations*/

             $table->foreign('ingreso_id')->references('id')->on('ingresos')
             ->onDelete('cascade')
             ->onUpdate('cascade');

             $table->foreign('producto_id')->references('id')->on('products')
             ->onDelete('cascade')
             ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_ingresos');
    }
}
